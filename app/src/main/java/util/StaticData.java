package util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import android.widget.Toast;

import net.marusoft.common.Register_CallBackItem;
import net.marusoft.common.SearchMe_APIService;
import net.marusoft.search_me.APIService;
import net.marusoft.search_me.Address_Data;

import java.io.File;
import java.util.regex.Pattern;

import main_register.Register_Data;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by MARUSOFT-CHOI on 2016-02-01.
 */
public class StaticData {
    public static boolean refresh_bool = false;
    public static final String DAUMMAPKEY = "39e968f9ad89ba2f24c0b816917fd995";
    public static final String API_URL = "https://apis.daum.net";
    public static final String IMG_URL = "http://serchme.cafe24.com/img/";
    public static final String PROVISION_URL = "http://serchme.cafe24.com/location_provision.html";
    public static final String SEARCHME_URL = "http://serchme.cafe24.com/api/";
    public static Address_Data address_data = new Address_Data();
    //public static double finalLongitude = 0.0, finalLatitude = 0.0;
    public static RbPreference pre;
    public static Address_Data get_gpsjson(final Context context, final TextView textview, final double longitude, final double latitude) {
        pre = new RbPreference(context);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Address_Data> call = service.addressdata(DAUMMAPKEY, longitude, latitude, "WGS84", "json");
        call.enqueue(new Callback<Address_Data>() {
            @Override
            public void onResponse(Response<Address_Data> response, Retrofit retrofit) {
                if(response.isSuccess()) {
                    address_data = response.body();
                    MyLogs.v("retrofit data : " + address_data.toString());
                    textview.setText(address_data.getFullName());
                    pre.put(pre.CURRENT_ADDRESS, address_data.getFullName());
                    putFixlocation(context, address_data, String.valueOf(latitude), String.valueOf(longitude));
                    String user_num = pre.getValue(RbPreference.USER_NUM, "");
                    MyLogs.v("user_num_static : " + user_num);
                    if(user_num.equals("")) {
                        putUserRegister(context, address_data, String.valueOf(latitude), String.valueOf(longitude));
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                MyLogs.v("Fail");
            }
        });
        return address_data;
    }

    public static void putFixlocation(final Context context, Address_Data address_data, String latitude, String longitude) {
        MyProgressDialog.showDialog(context, "위치 변경하는중...");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SearchMe_APIService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SearchMe_APIService service = retrofit.create(SearchMe_APIService.class);
        Call<Register_CallBackItem> call = service.user_location_register(pre.getValue(RbPreference.USER_NUM, ""), address_data.getRegionId(), address_data.getFullName(), "n", latitude, longitude);
        call.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                if (response.body().isResponseSuccess()) {
                    //Toast.makeText(context, "위치 변경이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                } else {
                    Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    static void putUserRegister(final Context context, Address_Data address_data, final String latitude, String longitude) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SearchMe_APIService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SearchMe_APIService service = retrofit.create(SearchMe_APIService.class);
        Call<Register_Data> callback = service.user_register(pre.getValue(pre.USER_PHONENUM, ""), pre.getValue(RbPreference.USER_NAME, ""), address_data.getFullName(), address_data.getRegionId(), pre.getValue(pre.REGID, ""), pre.getValue(pre.DEVICEID, ""), latitude, longitude);
        //MyLogs.v("preference_data : "  + pre.getValue(RbPreference.USER_NAME, "") + address_data.getData().getFullName() + location_data.getData().getRegionId());
        callback.enqueue(new Callback<Register_Data>() {
            @Override
            public void onResponse(Response<Register_Data> response, Retrofit retrofit) {
                StaticData.initPreference(context).put(RbPreference.ACTIVITYCHOICE, 0);
                Toast.makeText(context, "등록이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                Register_Data data = response.body();
                MyLogs.v("register_data : " + data.meta.getData()[0].toString());
                pre.put(pre.USER_NUM, data.meta.getData()[0]);
                pre.put(pre.USER_PHONENUM, data.meta.getData()[2]);
            }
            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context, "등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static RbPreference initPreference(Context context) {
        if(pre == null) {
            return pre = new RbPreference(context);
        } else {
            return pre;
        }
    }

    public static String getphoneNum(Context context) {
        TelephonyManager telManager = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
        String phoneNum = telManager.getLine1Number();
        if(phoneNum.startsWith("+82")){
            phoneNum = phoneNum.replace("+82", "0");
        }
        /*int size = phoneNum.length() - 3;
        StringBuffer sb = new StringBuffer(phoneNum); //생성
        sb.insert(3, "-");
        sb.insert(size, "-");*/
        return phoneNum;
    }

    /*public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                MyLogs.v("cache_in");
                deleteDir(dir);
            }
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }*/


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }
        else if(dir!= null && dir.isFile())
            return dir.delete();
        else {
            return false;
        }
    }

    public static String makePhoneNumber(String phoneNumber) {

        String regEx = "(\\d{3})(\\d{3,4})(\\d{4})";

        if(!Pattern.matches(regEx, phoneNumber)) return null;

        return phoneNumber.replaceAll(regEx, "$1-$2-$3");
    }

}
