package util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class RbPreference {

    private final String PREF_NAME = "com.rabiaband.pref";

    public final static String PREF_INTRO_USER_AGREEMENT = "PREF_USER_AGREEMENT";
    public final static String PREF_MAIN_VALUE = "PREF_MAIN_VALUE";
    public final static String USER_PHONENUM = "USER_PHONENUM";
    public final static String USER_NUM = "USER_NUM";
    public final static String USER_NAME = "USER_NAME";
    public final static String MAIN_ADDRESS = "MAIN_ADDRESS";
    public final static String CURRENT_ADDRESS = "CURRENT_ADDRESS";
    public final static String LOGIN_STATE = "LOGIN_STATE";
    public final static String PUSH_STATE = "PUSH_STATE";
    public final static String ACTIVITYCHOICE = "ACTIVITYCHOICE"; //0이면 초기등록창에서, 1이면 메인페이지에서 설정 넘김
    public final static String LASTLATITUDE = "LASTLATITUDE";
    public final static String LASTLONGITUDE = "LASTLONGITUDE";
    public final static String CHECK1 = "CHECK1";
    public final static String CHECK2 = "CHECK2";
    public final static String CHECK3 = "CHECK3";
    public final static String CHECK4 = "CHECK4";
    public final static String CHECK5 = "CHECK5";
    public final static String CHECK6 = "CHECK6";
    public final static String CHECK7 = "CHECK7";
    public final static String REGID = "REGID";
    public final static String DEVICEID = "DEVICEID";
    static Context mContext;

    public RbPreference(Context c) {
        mContext = c;
    }


    public void put(String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString(key, value);
        editor.commit();
    }

    public void put(String key, boolean value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    public void put(String key, int value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(key, value);
        editor.commit();
    }

    public void put(String key, float value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putFloat(key, value);
        editor.commit();
    }

    public void put(String key, long value) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putLong(key, value);
        editor.commit();
    }

    public String getValue(String key, String dftValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);

        try {
            return pref.getString(key, dftValue);
        } catch (Exception e) {
            return dftValue;
        }

    }

    public int getValue(String key, int dftValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);

        try {
            return pref.getInt(key, dftValue);
        } catch (Exception e) {
            return dftValue;
        }

    }

    public boolean getValue(String key, boolean dftValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);

        try {
            return pref.getBoolean(key, dftValue);
        } catch (Exception e) {
            return dftValue;
        }
    }

    public float getValue(String key, float dftValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);

        try {
            return pref.getFloat(key, dftValue);
        } catch (Exception e) {
            return dftValue;
        }
    }

    public float getValue(String key, long dftValue) {
        SharedPreferences pref = mContext.getSharedPreferences(PREF_NAME,
                Activity.MODE_PRIVATE);

        try {
            return pref.getLong(key, dftValue);
        } catch (Exception e) {
            return dftValue;
        }
    }
}

