package util;

import android.content.Context;

/**
 * Created by SEO on 2016-02-04.
 */
public class RbPreferenceManager {

     public static String getUserId(Context context){
         return StaticData.initPreference(context).getValue(RbPreference.USER_NUM, "");
    }

    public static String getUserName(Context context){
        return StaticData.initPreference(context).getValue(RbPreference.USER_NAME,"");
    }

    public static String getUserLastAddress(Context context){
        return StaticData.initPreference(context).getValue(RbPreference.CURRENT_ADDRESS, "");
    }

    public static String getUserLastLatitude(Context context){
        return StaticData.initPreference(context).getValue(RbPreference.LASTLATITUDE, "");
    }

    public static String getUserLastLongitude(Context context){
        return StaticData.initPreference(context).getValue(RbPreference.LASTLONGITUDE, "");
    }
}
