package util;

import net.marusoft.search_me.Address_Data;

import java.io.Serializable;

/**
 * Created by MARUSOFT-CHOI on 2016-02-01.
 */
public class LocationData implements Serializable {
    Address_Data data;
    String l_longitude;
    String l_latitude;

    public LocationData(Address_Data data, String l_longitude, String l_latitude) {
        this.data = data;
        this.l_longitude = l_longitude;
        this.l_latitude = l_latitude;
    }

    public Address_Data getData() {
        return data;
    }

    public void setData(Address_Data data) {
        this.data = data;
    }

    public String getL_longitude() {
        return l_longitude;
    }

    public void setL_longitude(String l_longitude) {
        this.l_longitude = l_longitude;
    }

    public String getL_latitude() {
        return l_latitude;
    }

    public void setL_latitude(String l_latitude) {
        this.l_latitude = l_latitude;
    }

    @Override
    public String toString() {
        return "LocationData{" +
                "data=" + data +
                ", l_longitude='" + l_longitude + '\'' +
                ", l_latitude='" + l_latitude + '\'' +
                '}';
    }
}
