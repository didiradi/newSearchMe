package util;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.ContextThemeWrapper;

public class MyProgressDialog {
static ProgressDialog progressDialog;
	public static void showDialog(Context context,String message){
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
//			if(progressDialog == null){
				  progressDialog = new ProgressDialog(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog));
//			}
			 
			   progressDialog.setMessage(message);
			  
			}else{
//				if(progressDialog == null){
					  progressDialog = new ProgressDialog(context);
//				}
			 
			   progressDialog.setMessage(message);
			}
		 progressDialog.show();
//		 progressDialog.dismiss()
	}
	public static void disMissDialog(){
		if(progressDialog!=null && (progressDialog.isShowing()==true)){
			progressDialog.dismiss();
		}
		
	}
	
	
	

}
