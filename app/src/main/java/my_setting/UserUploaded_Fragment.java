package my_setting;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.marusoft.common.BaseFragment;
import net.marusoft.common.CommonSearchMe_List_Adapter;
import net.marusoft.common.Common_List_Adapter;
import net.marusoft.common.Common_List_Data;
import net.marusoft.common.Common_SearchMeItem_CallBack;
import net.marusoft.common.EndlessRecyclerOnScrollListener;
import net.marusoft.common.Post_Detail_Item_Data;
import net.marusoft.lost_and_found.LostAndFound_register_Activity;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;
import util.RbPreferenceManager;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28. 분실물 프래그먼트
 */
public class UserUploaded_Fragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private int index = 0;
    private int count = 20;
    boolean allItemLoaded = false;
    CommonSearchMe_List_Adapter commonSearchMeListAdapter;
    private SwipeRefreshLayout mSwipeRefresh;
    public UserUploaded_Fragment newInstance() {
        UserUploaded_Fragment fragment = new UserUploaded_Fragment();
        return fragment;
    }

    @Bind(R.id.cl_recycleView)
    RecyclerView cl_recycleView;
    Common_List_Adapter adapter;
    ArrayList<Common_List_Data> data_list = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        MyLogs.v("resume_up");
        allItemLoaded = false;
        index = 0;
        put_userupload();
    }

    @Override
    public void onRefresh() {
        allItemLoaded = false;
        index = 0;
        put_userupload();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            setHasOptionsMenu(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.common_list_fragment, container, false);
        ButterKnife.bind(this, v);

        cl_recycleView.setHasFixedSize(true);
        cl_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swype);
        mSwipeRefresh.setOnRefreshListener(this);

        return v;
    }


    void put_userupload() {
        mSwipeRefresh.setRefreshing(true);
        Call<Common_SearchMeItem_CallBack> call = service.user_post_list(RbPreferenceManager.getUserId(getActivity()),index,count);
        call.enqueue(new Callback<Common_SearchMeItem_CallBack>() {
            @Override
            public void onResponse(Response<Common_SearchMeItem_CallBack> response, Retrofit retrofit) {
                ArrayList<Post_Detail_Item_Data> dataArrayList = (ArrayList<Post_Detail_Item_Data>)response.body().getData();
                commonSearchMeListAdapter = new CommonSearchMe_List_Adapter(getActivity(),dataArrayList,true);
                cl_recycleView.setAdapter(commonSearchMeListAdapter);
                index = dataArrayList.size();
                mSwipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        cl_recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                Call<Common_SearchMeItem_CallBack> call = service.user_post_list(RbPreferenceManager.getUserId(getActivity()),index,count);
                if(allItemLoaded==false) {

                    call.enqueue(new Callback<Common_SearchMeItem_CallBack>() {
                        @Override
                        public void onResponse(Response<Common_SearchMeItem_CallBack> response, Retrofit retrofit) {
                            MyLogs.v("index : "+index+" count : "+count);
                            ArrayList<Post_Detail_Item_Data> dataArrayList = (ArrayList<Post_Detail_Item_Data>) response.body().getData();


                            if (dataArrayList.size() == 0) {
                                allItemLoaded = true;
                                //Toast.makeText(getActivity(),"마지막 글입니다",Toast.LENGTH_LONG).show();
                            } else {
                                commonSearchMeListAdapter.addItems(dataArrayList);
                                index = commonSearchMeListAdapter.getItemCount()-1;
                            }
                            mSwipeRefresh.setRefreshing(false);
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                }
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.board_register_fragment_menu, menu);
    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.register_btn) {
            Intent intent = new Intent(getActivity(),LostAndFound_register_Activity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
