package my_setting;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import net.marusoft.common.BaseFragment;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.MainActivity;
import net.marusoft.search_me.MapSelectActivity;
import net.marusoft.search_me.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import main_register.Phone_Register_Activity;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.LocationData;
import util.MyLogs;
import util.MyProgressDialog;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-02.
 */
public class Setting_Fragment extends BaseFragment {
    @Bind(R.id.push_layout)
    LinearLayout push_layout;
    @Bind(R.id.push_switch)
    Switch push_switch;
    @Bind(R.id.check1)
    CheckBox check1;
    @Bind(R.id.check2)
    CheckBox check2;
    @Bind(R.id.check3)
    CheckBox check3;
    @Bind(R.id.check4)
    CheckBox check4;
    @Bind(R.id.check5)
    CheckBox check5;
    @Bind(R.id.check6)
    CheckBox check6;
    @Bind(R.id.check7)
    CheckBox check7;
    @Bind(R.id.phone_name_tv)
    TextView phone_name_tv;
    @Bind(R.id.location_modify_tv)
    TextView location_modify_tv;
    @Bind(R.id.location_current_tv)
    TextView location_current_tv;
    final static int CHECKBOX_SIZE = 7;
    String check_bool[] = new String[CHECKBOX_SIZE];
    private LocationData location_data;
    private RbPreference pre;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_fragment, container, false);
        ButterKnife.bind(this, v);
        Initialize();

        phone_name_tv.setText(pre.getValue(pre.USER_PHONENUM, "") + ", " + pre.getValue(pre.USER_NAME, ""));
        location_current_tv.setText(pre.getValue(pre.CURRENT_ADDRESS, "현재위치 설정 필요"));
        location_modify_tv.setText(pre.getValue(pre.MAIN_ADDRESS, "주사용위치 설정 필요"));
        return v;
    }

    void Initialize() {
        pre = new RbPreference(getActivity());
        push_switch.setChecked(pre.getValue(pre.PUSH_STATE, true));
        if(pre.getValue(pre.PUSH_STATE, true)) {
            push_layout.setVisibility(View.VISIBLE);
        } else {
            push_layout.setVisibility(View.GONE);
        }
        check1.setChecked(pre.getValue(pre.CHECK1, false));
        check2.setChecked(pre.getValue(pre.CHECK2, false));
        check3.setChecked(pre.getValue(pre.CHECK3, false));
        check4.setChecked(pre.getValue(pre.CHECK4, false));
        check5.setChecked(pre.getValue(pre.CHECK5, false));
        check6.setChecked(pre.getValue(pre.CHECK6, false));
        check7.setChecked(pre.getValue(pre.CHECK7, false));
    }

    @OnCheckedChanged(R.id.push_switch)
    void onChecked(boolean isChecked) {
        if (isChecked) {
            push_layout.setVisibility(View.VISIBLE);
            pre.put(pre.PUSH_STATE, true);
            push_switch.setChecked(pre.getValue(pre.PUSH_STATE, true));
            MyLogs.v("radio" + pre.getValue(pre.PUSH_STATE, true));

        } else {
            push_layout.setVisibility(View.GONE);
            pre.put(pre.PUSH_STATE, false);
            push_switch.setChecked(pre.getValue(pre.PUSH_STATE, false));
            MyLogs.v("radio" + pre.getValue(pre.PUSH_STATE, true));

        }
    }

    @OnClick(R.id.location_provision_tv)
    void location_provision_tv() {
        Intent intent = new Intent(getActivity(), Location_Provision_Activity.class);
        startActivity(intent);
    }
    @OnClick(R.id.logout_btn)
    void logout_btn(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("로그아웃");
        dialog.setMessage("로그아웃 하시겠습니까?");
        dialog.setPositiveButton("예", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                pre.put(pre.LOGIN_STATE, false);
                Toast.makeText(getActivity(), "로그아웃 되었습니다.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), Phone_Register_Activity.class);
                startActivity(intent);
                getActivity().finish();
                //인텐트로 처음화면넘기기
            }
        });
        dialog.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.create().show();
    }
    @OnClick(R.id.location_modify_btn)
    void location_modify_btn() {
        Intent intent = new Intent(getActivity(), MapSelectActivity.class);
        startActivityForResult(intent, 1);
    }
    @OnClick(R.id.gps_btn)
    void gps_btn() {
        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        startActivityForResult(intent, 0);
    }
    @OnClick(R.id.register_btn)
    void register_btn() {
        if(check1.isChecked()) {
            check_bool[0] = "y";
            pre.put(pre.CHECK1, true);
        } else {
            check_bool[0] = "n";
            pre.put(pre.CHECK1, false);
        }
        if(check2.isChecked()) {
            check_bool[1] = "y";
            pre.put(pre.CHECK2, true);
        } else {
            check_bool[1] = "n";
            pre.put(pre.CHECK2, false);
        }
        if(check3.isChecked()) {
            check_bool[2] = "y";
            pre.put(pre.CHECK3, true);
        } else {
            check_bool[2] = "n";
            pre.put(pre.CHECK3, false);
        }
        if(check4.isChecked()) {
            check_bool[3] = "y";
            pre.put(pre.CHECK4, true);
        } else {
            check_bool[3] = "n";
            pre.put(pre.CHECK4, false);
        }
        if(check5.isChecked()) {
            check_bool[4] = "y";
            pre.put(pre.CHECK5, true);
        } else {
            check_bool[4] = "n";
            pre.put(pre.CHECK5, false);
        }
        if(check6.isChecked()) {
            check_bool[5] = "y";
            pre.put(pre.CHECK6, true);
        } else {
            check_bool[5] = "n";
            pre.put(pre.CHECK6, false);
        }
        if(check7.isChecked()) {
            check_bool[6] = "y";
            pre.put(pre.CHECK7, true);
        } else {
            check_bool[6] = "n";
            pre.put(pre.CHECK7, false);
        }

        Call<Register_CallBackItem> callback = service.push_check(StaticData.initPreference(getActivity()).getValue(RbPreference.USER_NUM, ""), check_bool[0], check_bool[1], check_bool[2], check_bool[3], check_bool[4], check_bool[5], check_bool[6]);
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                if (response.body().isResponseSuccess()) {
                    Toast.makeText(getActivity(), "푸쉬 설정이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    if (StaticData.initPreference(getActivity()).getValue(RbPreference.ACTIVITYCHOICE, 0) == 0) {
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    } else if (StaticData.initPreference(getActivity()).getValue(RbPreference.ACTIVITYCHOICE, 0) == 1) {
                        getActivity().finish();
                    }
                } else {
                    Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @OnClick(R.id.cancel_btn)
    void cancel_btn() {
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if(resultCode == getActivity().RESULT_OK) {
                    location_data =(LocationData)data.getSerializableExtra("location_data");
                    pre.put(pre.MAIN_ADDRESS, location_data.getData().getFullName());
                    location_modify_tv.setText(location_data.getData().getFullName());
                    putFixlocation();
                }
                break;
            default:

                break;
        }
    }

    void putFixlocation() {
        MyProgressDialog.showDialog(getActivity(), "주 위치 변경하는중...");
        Call<Register_CallBackItem> callback = service.user_location_register(pre.getValue(RbPreference.USER_NUM, ""), location_data.getData().getRegionId(), location_data.getData().getFullName(), "y", location_data.getL_latitude(), location_data.getL_longitude());
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                if (response.body().isResponseSuccess()) {
                    Toast.makeText(getActivity(), "주 위치 변경이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                } else {
                    Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
