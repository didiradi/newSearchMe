package my_setting;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import net.marusoft.common.BaseActivity;
import net.marusoft.search_me.R;

/**
 * Created by MARUSOFT-CHOI on 2016-02-02.
 */
public class My_Setting_Activity extends BaseActivity {
    private ViewPager viewPager;
    public TabLayout tabLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_viewpager);
        Initialize();
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount()); // page 확정
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        UserUploaded_Fragment userUploaded_fragment;
        Setting_Fragment setting_fragment;
        final int PAGE_COUNT = 2;

        private final String[] TITLES = {"마이 써치", "설정"};

        public PagerAdapter(FragmentManager fm) {
            super(fm);

             userUploaded_fragment = new UserUploaded_Fragment();
             setting_fragment = new Setting_Fragment();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return userUploaded_fragment;
            } else {
                return setting_fragment;
            }
        }
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }
}
