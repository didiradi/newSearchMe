package main_register;

import java.util.Arrays;

/**
 * Created by MARUSOFT-CHOI on 2016-02-02.
 */
public class Register_Data {

    public Meta meta;

    public class Meta {
        String code;
        String state;
        String msg;
        String data[];

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String[] getData() {
            return data;
        }

        public void setData(String[] data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "meta{" +
                    "code='" + code + '\'' +
                    ", state='" + state + '\'' +
                    ", msg='" + msg + '\'' +
                    ", data=" + Arrays.toString(data) +
                    '}';
        }
    }
}
