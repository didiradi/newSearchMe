package main_register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class Phone_Register_Activity extends BaseActionbarActivity {
    @Bind(R.id.phone_et)
    EditText phone_et;
    private Context context;
    private String phoneNum;
    private String getPhone;
    private int phoneNum_size = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_register_activity);
        context = Phone_Register_Activity.this;
        ButterKnife.bind(this);
        Initialize();
        //MyLogs.v("phone : " + StaticData.getphoneNum(context));

        getPhone = StaticData.getphoneNum(context);
        phone_et.setText(getPhone);
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.register_btn)
    void register_btn() {
        if(phone_et.getText().toString().length() == 10 || phone_et.getText().toString().length() == 11) {
            phoneNum_size = StaticData.makePhoneNumber(phone_et.getText().toString()).length();
            MyLogs.v("phonesize : " + phoneNum_size);
            phoneNum = StaticData.makePhoneNumber(phone_et.getText().toString());
            MyLogs.v("phonesize : " + phoneNum);
            Call<Register_CallBackItem> callback = service.phone_check(phoneNum);
            callback.enqueue(new Callback<Register_CallBackItem>() {
                @Override
                public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                    if (response.body().isResponseSuccess()) {
                        Toast.makeText(context, "핸드폰으로 문자가 발송됩니다. 잠시만 기다려주세요.", Toast.LENGTH_SHORT).show();
                        StaticData.initPreference(context).put(RbPreference.USER_PHONENUM, phoneNum);
                        Intent intent = new Intent(context, Auth_Register_Activity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(context, "잠시 후 다시 이용해주세요.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Toast.makeText(context, "인증 서버에서 에러발생.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(context, "핸드폰 번호를 형식에 맞게 올바르게 입력해주세요.", Toast.LENGTH_LONG).show();
        }

    }
}
