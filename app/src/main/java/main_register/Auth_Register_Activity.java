package main_register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.R;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;
import util.RbPreference;
import util.SMSReceiver;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class Auth_Register_Activity extends BaseActionbarActivity {
    private static EditText auth_et;
    private Button register_btn;
    private Context context;
    SMSReceiver sms = new SMSReceiver();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_register_activity);
        context = Auth_Register_Activity.this;
        Initialize();
        auth_et = (EditText) findViewById(R.id.auth_et);
        register_btn = (Button) findViewById(R.id.register_btn);

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyLogs.v("phone data : " + StaticData.initPreference(context).getValue(RbPreference.USER_PHONENUM, "") + " " + auth_et.getText().toString());
                Call<Register_CallBackItem> callback = service.phone_auth_check(StaticData.initPreference(context).getValue(RbPreference.USER_PHONENUM, ""), auth_et.getText().toString().trim());
                callback.enqueue(new Callback<Register_CallBackItem>() {
                    @Override
                    public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                        if(response.body().isResponseSuccess()){
                            Toast.makeText(context, "인증이 승인되었습니다.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, Username_Register_Activity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(context, "인증번호가 틀렸습니다. 다시 입력해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(context, "인증 서버에서 에러발생.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }
    // SMSReceiver에서 접근해야하기 때문에 static
    public static void inputAuthNumber(String authNumber) {
        if (authNumber != null) {
            // editTextInputNumber에 받아온 인증번호를 입력
            if(auth_et != null) {
                auth_et.setText(authNumber);
            }
        }
    }
}
