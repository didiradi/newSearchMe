package main_register;

/**
 * Created by MARUSOFT-CHOI on 2016-02-04.
 */
public class User_Data {
    public Meta meta;
    public Data data;
    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public class Meta {
        String code;
        String state;
        String msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        String u_id;
        String u_name;
        String u_phone;
        String u_write_date;
        String u_address;

        public String getU_id() {
            return u_id;
        }

        public void setU_id(String u_id) {
            this.u_id = u_id;
        }

        public String getU_name() {
            return u_name;
        }

        public void setU_name(String u_name) {
            this.u_name = u_name;
        }

        public String getU_phone() {
            return u_phone;
        }

        public void setU_phone(String u_phone) {
            this.u_phone = u_phone;
        }

        public String getU_write_date() {
            return u_write_date;
        }

        public void setU_write_date(String u_write_date) {
            this.u_write_date = u_write_date;
        }

        public String getU_address() {
            return u_address;
        }

        public void setU_address(String u_address) {
            this.u_address = u_address;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "u_id='" + u_id + '\'' +
                    ", u_name='" + u_name + '\'' +
                    ", u_phone='" + u_phone + '\'' +
                    ", u_write_date='" + u_write_date + '\'' +
                    ", u_address='" + u_address + '\'' +
                    '}';
        }
    }
}
