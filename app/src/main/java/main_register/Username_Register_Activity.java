package main_register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.MainActivity;
import net.marusoft.search_me.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-02.
 */
public class Username_Register_Activity extends BaseActionbarActivity {
    @Bind(R.id.name_et)
    EditText name_et;
    private Context context;
    private RbPreference pre;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.username_register_activity);
        context = Username_Register_Activity.this;
        ButterKnife.bind(this);
        Initialize();
        pre = new RbPreference(context);
        getPhoneCheck(); //이미 가입된 번호인지 체크
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.register_btn)
    void register_btn() {
        if(name_et.getText().length() < 1) {
            Toast.makeText(context, "서치미에서 사용할 이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
        } else {
            Call<Register_CallBackItem> callback = service.user_name_check(name_et.getText().toString().trim());
            callback.enqueue(new Callback<Register_CallBackItem>() {
                @Override
                public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                    if(response.body().isResponseSuccess()){
                        Toast.makeText(context, "등록이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                        StaticData.initPreference(context).put(RbPreference.LOGIN_STATE, true);
                        StaticData.initPreference(context).put(RbPreference.USER_NAME, name_et.getText().toString());
                        Intent intent = new Intent(context, Location_Register_Activity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(context, "이미 등록된 이름입니다. 다른 이름을 사용해주세요", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }
    @OnClick(R.id.cancel_btn)
    void cancel_btn() {
        finish();
    }

    public void getPhoneCheck() {
        Call<Register_CallBackItem> callback = service.phone_duplication_check(StaticData.initPreference(context).getValue(RbPreference.USER_PHONENUM, ""));
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                if (response.body().isResponseSuccess()) {

                } else {
                    Toast.makeText(context, "이미 등록된 번호입니다.", Toast.LENGTH_SHORT).show();
                    StaticData.initPreference(context).put(RbPreference.LOGIN_STATE, true);
                    getUserData();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public void getUserData() {
        Call<User_Data> callback = service.get_user_data(StaticData.initPreference(context).getValue(RbPreference.USER_PHONENUM, ""));
        callback.enqueue(new Callback<User_Data>() {
            @Override
            public void onResponse(Response<User_Data> response, Retrofit retrofit) {
                MyLogs.v("user_data : " + response.body().getData().toString());
                pre.put(pre.USER_NAME, response.body().getData().getU_name());
                pre.put(pre.USER_NUM, response.body().getData().getU_id());
                pre.put(pre.USER_PHONENUM, response.body().getData().getU_phone());
                pre.put(pre.MAIN_ADDRESS, response.body().getData().getU_address());
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
