package main_register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Toast;

import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.search_me.MapSelectActivity;
import net.marusoft.search_me.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my_setting.My_Setting_Activity;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.LocationData;
import util.MyLogs;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-02.
 */
public class Location_Register_Activity extends BaseActionbarActivity {
    private Context context;
    @Bind(R.id.lr_btn)
    Button lr_btn;
    @Bind(R.id.register_btn)
    Button register_btn;
    @Bind(R.id.cancel_btn)
    Button cancel_btn;
    private LocationData location_data;
    private RbPreference pre;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_register_activity);
        context = Location_Register_Activity.this;
        ButterKnife.bind(this);
        Initialize();
        pre = new RbPreference(context);
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }
    @OnClick(R.id.lr_btn)
    void lr_btn() {
        Intent intent = new Intent(context, MapSelectActivity.class);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.register_btn)
    void register_btn() {
        Call<Register_Data> callback = service.user_register(pre.getValue(pre.USER_PHONENUM, ""), pre.getValue(RbPreference.USER_NAME, ""), location_data.getData().getFullName(), location_data.getData().getRegionId().trim(), pre.getValue(pre.REGID, ""), pre.getValue(pre.DEVICEID, ""), location_data.getL_latitude(), location_data.getL_longitude());
        MyLogs.v("preference_data : "  + pre.getValue(RbPreference.USER_NAME, "") + location_data.getData().getFullName() + location_data.getData().getRegionId());
        callback.enqueue(new Callback<Register_Data>() {
            @Override
            public void onResponse(Response<Register_Data> response, Retrofit retrofit) {
                    StaticData.initPreference(context).put(RbPreference.ACTIVITYCHOICE, 0);
                    Toast.makeText(context, "등록이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    Register_Data data = response.body();
                    MyLogs.v("register_data : " + data.meta.getData()[0].toString());
                    pre.put(pre.USER_NUM, data.meta.getData()[0]);
                    pre.put(pre.USER_PHONENUM, data.meta.getData()[2]);
                    Intent intent = new Intent(context, My_Setting_Activity.class);
                    startActivity(intent);
                    finish();
            }
            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context, "등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
        /*Intent intent = new Intent(context, MainActivity.class);
        startActivity(intent);*/
    }
    @OnClick(R.id.cancel_btn)
    void cancel_btn() {
        finish();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if(resultCode == RESULT_OK) {
                    location_data =(LocationData)data.getSerializableExtra("location_data");
                    pre.put(pre.MAIN_ADDRESS, location_data.getData().getFullName());
                    lr_btn.setText(location_data.getData().getFullName());
                }
                break;
            default:

                break;
        }
    }
}
