package net.marusoft.pet;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.common.Category_Sub_Item;
import net.marusoft.common.Common_CallBackItem;
import net.marusoft.common.DetailImagePagerAdapter;
import net.marusoft.common.Post_Detail_Item_Data;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.CustomCircleIndicater;
import util.MyLogs;
import util.RbPreference;
import util.StaticData;

/**
 * Created by  Henen on 2016-02-04.
 */
public class PetPreview_Activity extends BaseActionbarActivity {
    public static final String TAG = "PET_PREVIEW_FRAGMENT";
    private static final String ARG_POST_DETAIL_ITEM= "ARG_POST_DETAIL_ITEM";
    private static final String ARG_SUB_CATEGORY_ITEM= "ARG_SUB_CATEGORY_ITEM";
    private Post_Detail_Item_Data param_post_detail_item;
    private Category_Sub_Item param_sub_category_item;

    @Bind(R.id.title_tv) TextView title_tv;
    @Bind(R.id.lost_property_tv) TextView lost_property_tv;
    @Bind(R.id.registerName_tv) TextView registerName_tv;
    @Bind(R.id.feature_tv) TextView feature_tv;
    @Bind(R.id.location_detail_tv) TextView location_detail_tv;
    @Bind(R.id.happenDate_tv) TextView happenDate_tv;
    @Bind(R.id.money_tv) TextView money_tv;
    @Bind(R.id.content_tv) TextView content_tv;
    @Bind(R.id.safenumber_button)  Button safeNumberBtn;


    private Context context;
    private DetailImagePagerAdapter mImagePagerAdapter;
    private AutoScrollViewPager mViewPager;
    private ArrayList<String> img_list;
    private CustomCircleIndicater mCirclePageIndicator;

    /*public LostAndFoundPreview_Fragment newInstance(Post_Detail_Item_Data post_detail_item_data,Category_Sub_Item category_sub_item){
        LostAndFoundPreview_Fragment lostAndFoundPreview_fragment = new LostAndFoundPreview_Fragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POST_DETAIL_ITEM, post_detail_item_data);
        args.putSerializable(ARG_SUB_CATEGORY_ITEM, category_sub_item);
        lostAndFoundPreview_fragment.setArguments(args);
        return lostAndFoundPreview_fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pet_preview_fragment);
        ButterKnife.bind(this);
        context = this;
        param_post_detail_item = (Post_Detail_Item_Data) getIntent().getSerializableExtra(ARG_POST_DETAIL_ITEM);
        param_sub_category_item = (Category_Sub_Item)getIntent().getSerializableExtra(ARG_SUB_CATEGORY_ITEM);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        mCirclePageIndicator = (CustomCircleIndicater) findViewById(R.id.indicator);
        mCirclePageIndicator.setCircleExtraSpace(25);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        collapsingToolbarLayout.setTitle("등록 미리보기");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(param_post_detail_item.getPhoto() != null) {

            img_list = new ArrayList<String>();

            int photo_size =param_post_detail_item.getPhoto().size();

            findViewById(R.id.empty_view).setVisibility(View.GONE);

            // MyLogs.v("post_detail_item : " + item.getData().getP_category_id() + " : " + item.getData().getPhoto().get(0).getPp_img_name());
            for(int i=0; i<photo_size; i++) {
                img_list.add(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
            }

            mImagePagerAdapter = new DetailImagePagerAdapter(this, img_list);
            mViewPager = (AutoScrollViewPager)findViewById(R.id.pager);
            mViewPager.setAdapter(mImagePagerAdapter);

            mCirclePageIndicator.setViewPager(mViewPager);

        } else {
            findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
        }

        title_tv.setText(param_post_detail_item.getP_title());
        lost_property_tv.setText("품종 : " + param_post_detail_item.getP_lost_property());
        registerName_tv.setText("등록자 : " + StaticData.initPreference(context).getValue(RbPreference.USER_NAME, ""));
        feature_tv.setText("특징 : " + param_post_detail_item.getP_feature());
        location_detail_tv.setText("지역 : " + param_post_detail_item.getP_location_detail_text());
        money_tv.setText("사례금 : " + param_post_detail_item.getP_money() + " 만원");
        happenDate_tv.setText("분실 일자 : " + param_post_detail_item.getP_happen_date());
        content_tv.setText("내용 : " + param_post_detail_item.getP_contents());

        safeNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPolicyDialog();


            }
        });
    }

    void showPolicyDialog(){
        String policy_text = "본인은 게시물 등록에 있어 아래 써치미의\n 이용약관에 동의하며, 모든 내용을 확인 하였습니다.\n" +
                "1. 해당 목적 외 다른 목적으로 사용하지 않으며, 이로\n" +
                "인한 문제 발생 시 모든 책임은 등록자 본인에게 있음\n" +
                "을 확인 합니다.\n" +
                "2. 등록하는 내용에 허위사실이 없으며, 만약 허위사\n" +
                "실을 등록한 경우 모든 법적 책임을 지겠습니다.\n" +
                "3. 본인의 핸드폰번호를 이용하여 안심번호를 생성하\n" +
                "는 것을 동의하며, 이를 악용하지 않겠습니다.\n" +
                "써치미는 플랫폼 제공사업자로서 당사자간에\n" +
                "발생할 수 있는 일에 대해서는 책임이 없습니다.\n" +
                "고객님의 휴대폰번호는 안전하게 안심번호로\n" +
                "변환되어 등록이 되며, 다른 회원님들이 고객님께\n" +
                "연락을 할 경우 안심번호로 연결이 됩니다.";
        View view = LayoutInflater.from(this).inflate(R.layout.policy_dialog, null);
        final TextView policy_textview =(TextView)view.findViewById(R.id.policy_textview);
        policy_textview.setText(policy_text);
        final ImageButton cancelBtn =(ImageButton)view.findViewById(R.id.cancel_btn);
        final Button policy_pre_agreeBtn =(Button)view.findViewById(R.id.policy_pre_agreement_btn);
        final Button policy_agreeBtn =(Button)view.findViewById(R.id.policy_agree_btn);
        policy_pre_agreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                policy_agreeBtn.setEnabled(true);
                policy_agreeBtn.setTextColor(Color.parseColor("#FFFFFF"));
                policy_agreeBtn.setBackgroundColor(Color.parseColor("#FFC000"));
            }
        });

        policy_agreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyLogs.v(param_sub_category_item.getC_name());
                Call<Register_CallBackItem> callback = service.pet_post_register(
                        param_sub_category_item.getC_id(),
                        param_sub_category_item.getC_name(),
                        param_sub_category_item.getC_number(),
                        param_post_detail_item.getP_user_id(),
                        param_post_detail_item.getP_title(),
                        param_post_detail_item.getP_money(),
                        param_post_detail_item.getP_lost_property(),
                        param_post_detail_item.getP_feature(),
                        param_post_detail_item.getP_contents(),
                        param_post_detail_item.getP_happen_date(),
                        param_post_detail_item.getP_location_id(),
                        param_post_detail_item.getP_location_text(),
                        param_post_detail_item.getP_location_detail_text(),
                        param_post_detail_item.getP_location_latitude(),
                        param_post_detail_item.getP_location_longitude());
                callback.enqueue(new Callback<Register_CallBackItem>() {
                    @Override
                    public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                        if (response.body().isResponseSuccess()) {
                            MyLogs.v(response.body().toString());

                            if (param_post_detail_item.getPhoto() != null && param_post_detail_item.getPhoto().size() > 0) {//파일존재
                                int post_id = response.body().getData().getPost_id();
                                RequestBody post_id_str =
                                        RequestBody.create(MediaType.parse("multipart/form-data"), "" + post_id);

                                for (int i = 0; i < param_post_detail_item.getPhoto().size(); i++) {
                                    MyLogs.v(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
                                    File file = new File(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
                                    MyLogs.v("file : " + file.getPath());
                                    RequestBody imageFile_body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    RequestBody order_id_str =
                                            RequestBody.create(MediaType.parse("multipart/form-data"), "" + (i + 1));
                                    Call<Common_CallBackItem> fileCallBack = service.post_photo_register(imageFile_body, order_id_str, post_id_str);

                                    fileCallBack.enqueue(new Callback<Common_CallBackItem>() {
                                        @Override
                                        public void onResponse(Response<Common_CallBackItem> f_response, Retrofit retrofit) {
                                            if (f_response.body().isResponseSuccess()) {
                                                MyLogs.v("Success");
                                                MyLogs.v(f_response.body().toString());
                                                Toast.makeText(context, "글 등록이 완료되었습니다.", Toast.LENGTH_LONG).show();
                                                StaticData.refresh_bool = true;
                                                finish();
                                                PetRegister_Activity.activity.finish();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            MyLogs.v("picture : " + t.getMessage().toString());
                                        }
                                    });


                                }
                            }// if(horizontalImageLayoutAdapter.getItemCount()>0){//파일존재
                            else {
                                Toast.makeText(context, "글 등록이 완료되었습니다.", Toast.LENGTH_LONG).show();
                                finish();
                                PetRegister_Activity.activity.finish();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MyLogs.v(t.getMessage().toString());
                    }
                });


            }
        });
        final AlertDialog ab= new AlertDialog.Builder(this)
                .setView(view)
                .create();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.dismiss();
            }
        });
        WindowManager.LayoutParams params = ab.getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        ab.getWindow().setAttributes((WindowManager.LayoutParams) params);
        ab.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }
}
