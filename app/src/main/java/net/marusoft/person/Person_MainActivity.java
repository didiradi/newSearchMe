package net.marusoft.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import net.marusoft.common.BaseActivity;
import net.marusoft.common.CategoryMananger;
import net.marusoft.common.CommonCategory;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import util.StaticData;

/**
 * Created by MARUSOFT-Henen on 2016-01-28.
 */
public class Person_MainActivity extends BaseActivity {
    public ViewPager viewPager;
    public TabLayout tabLayout;
    CommonCategory commonCategory;
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_activity);
        context =  Person_MainActivity.this;
        commonCategory = CategoryMananger.getCategoryFromNumberId(getApplicationContext(),CategoryMananger.TAG_PERSON);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount()); // page 확정
        tabLayout.setupWithViewPager(viewPager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.board_register_fragment_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            onBackPressed();
            return  true;
        }
        if (id == R.id.register_btn) {
            StaticData.refresh_bool = true; //페이지 다시 불러올 때 새로고침 하는지 여부
            Intent intent = new Intent(context, Person_register_Activity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.question_btn) {
            Toast.makeText(context, "준비중...", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public class PagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Person_Fragment> person_fragmentArrayList;

        final int PAGE_COUNT = commonCategory.sub_count();

        private final String[] TITLES = commonCategory.get_name_array();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            person_fragmentArrayList = new ArrayList<>();
            for(int i = 0  ; i<PAGE_COUNT;i++ ){
                person_fragmentArrayList.add(new Person_Fragment().newInstance(commonCategory.getSubCategoryItem(i).getC_number(),commonCategory.getSubCategoryItem(i).getC_id()));
            }
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {

            // TODO Auto-generated method stub
            super.destroyItem(container, position, object);

        }

        @Override
        public Fragment getItem(int position) {
            return person_fragmentArrayList.get(position);
        }

    }
}
