package net.marusoft.person;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import net.marusoft.search_me.R;

/**
 * Created by Henen on 2016-02-01.
 */
public class Person_register_Activity extends ActionBarActivity {
    Context context;
    public static ActionBarActivity activity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_register_activity);
        context = Person_register_Activity.this;
        activity = Person_register_Activity.this;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contents_linear, new PersonRegister_Fragment(), PersonRegister_Fragment.TAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public interface onKeyBackPressedListener {
        public void onBack();
    }

    private onKeyBackPressedListener mOnKeyBackPressedListener;

    public void setOnKeyBackPressedListener(onKeyBackPressedListener listener) {

        mOnKeyBackPressedListener = listener;
    }

    @Override

    public void onBackPressed() {

        if (mOnKeyBackPressedListener != null) {
            mOnKeyBackPressedListener.onBack();
        } else {
            super.onBackPressed();
        }

    } // in MyActivity
}
