package net.marusoft.detail_activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.marusoft.common.BaseActivity;
import net.marusoft.common.Favorite_Item;
import net.marusoft.common.ImagePagerAdapter;
import net.marusoft.common.Post_Detail_Item;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.common.safeNumData;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.CustomCircleIndicater;
import util.MyLogs;
import util.MyProgressDialog;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class Activity_LostPerson_Detail extends BaseActivity {
    @Bind(R.id.solved_tv) TextView solved_tv;
    @Bind(R.id.title_tv) TextView title_tv;
    @Bind(R.id.name_tv) TextView name_tv;
    @Bind(R.id.happenDate_tv) TextView happenDate_tv;
    @Bind(R.id.age_tv) TextView age_tv;
    @Bind(R.id.feature_tv) TextView feature_tv;
    @Bind(R.id.location_detail_tv) TextView location_detail_tv;
    @Bind(R.id.money_tv) TextView money_tv;
    @Bind(R.id.call_btn) Button call_btn;
    @Bind(R.id.solved_btn) Button solved_btn;
    private Toolbar toolbar;
    private Context context;
    private FloatingActionButton favorite_btn, map_btn;
    private ImagePagerAdapter mImagePagerAdapter;
    private AutoScrollViewPager mViewPager;
    private ArrayList<String> img_list;
    private Post_Detail_Item item;
    private String p_id;
    private CustomCircleIndicater mCirclePageIndicator;
    private boolean my_upload = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lostperson_detail);
        context = this;
        ButterKnife.bind(this);
        Initialize();

        getLostandFoundData();

        favorite_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getData().getP_isfavorite().equals("n")) {
                    MyLogs.v("관심글등록");
                    Toast.makeText(context, "관심글로 등록되었습니다.", Toast.LENGTH_SHORT).show();
                    favorite_btn.setImageResource(R.drawable.ht_on);
                    item.getData().setP_isfavorite("y");
                    putFavoriteData();
                } else if (item.getData().getP_isfavorite().equals("y")) {
                    MyLogs.v("관심글제거");
                    Toast.makeText(context, "관심글에서 제거 되었습니다.", Toast.LENGTH_SHORT).show();
                    favorite_btn.setImageResource(R.drawable.ht_off);
                    item.getData().setP_isfavorite("n");
                    deleteFavoriteData();
                }
            }
        });
        map_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Activity_Location.class);
                intent.putExtra("post_detail_item", item);
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.call_btn)
    void call_btn() {
        call_safephone();
    }

    @OnClick(R.id.solved_btn)
    void solved_btn() {
        put_solved();
    }

    void Initialize() {
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing);
        favorite_btn = (FloatingActionButton) findViewById(R.id.favorite_btn);
        map_btn = (FloatingActionButton) findViewById(R.id.map_btn);
        map_btn.setImageResource(R.drawable.map_img);
        mCirclePageIndicator = (CustomCircleIndicater) findViewById(R.id.indicator);
        mCirclePageIndicator.setCircleExtraSpace(25);
        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        collapsingToolbarLayout.setTitle("실종자 상세");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        p_id = getIntent().getExtras().getString("p_id");
        my_upload = getIntent().getExtras().getBoolean("my_upload");

        if(my_upload) { //내가 등록한 페이지에서 넘길때
            solved_btn.setVisibility(View.VISIBLE);
            call_btn.setVisibility(View.GONE);
        } else {
            solved_btn.setVisibility(View.GONE);
            call_btn.setVisibility(View.VISIBLE);
        }
    }

    void getLostandFoundData() {
        Call<Post_Detail_Item> callback = service.post_detail(StaticData.initPreference(context).getValue(RbPreference.USER_NUM, ""), p_id);
        callback.enqueue(new Callback<Post_Detail_Item>() {
            @Override
            public void onResponse(Response<Post_Detail_Item> response, Retrofit retrofit) {
                img_list = new ArrayList<String>();
                item = response.body();
                int photo_size = item.getData().getPhoto().size();

                MyLogs.v("post_detail_item : " + item.getMeta().toString());
                MyLogs.v("post_detail_item : " + item.getData().getP_category_id());
                MyLogs.v("favorite  : " + item.getData().getP_isfavorite());

                for (int i = 0; i < photo_size; i++) {
                    img_list.add(item.getData().getPhoto().get(i).getPp_img_name());
                }

                mImagePagerAdapter = new ImagePagerAdapter(context, img_list);
                mViewPager = (AutoScrollViewPager) findViewById(R.id.pager);
                mViewPager.setAdapter(mImagePagerAdapter);

                if (item.getData().getP_isfavorite().equals("y")) {
                    favorite_btn.setImageResource(R.drawable.ht_on);
                } else {
                    favorite_btn.setImageResource(R.drawable.ht_off);
                }

                setTextViewData();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    void putFavoriteData() {
        Call<Favorite_Item> callback = service.favorite_register(StaticData.initPreference(context).getValue(RbPreference.USER_NUM, ""), p_id);
        callback.enqueue(new Callback<Favorite_Item>() {
            @Override
            public void onResponse(Response<Favorite_Item> response, Retrofit retrofit) {
                MyLogs.v("favorite data : " + response.body().getData().getFavorite_id());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    void deleteFavoriteData() {
        Call<Register_CallBackItem> callback = service.favorite_delete(StaticData.initPreference(context).getValue(RbPreference.USER_NUM, ""), p_id);
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {

                MyLogs.v("delete ok : " + "잘지워짐");

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    void call_safephone() {
        MyProgressDialog.showDialog(context, "안심번호에 연결하는중..");
        Call<safeNumData> callback = service.post_phone_register(item.getData().getP_id(), item.getData().getP_user_id());
        callback.enqueue(new Callback<safeNumData>() {
            @Override
            public void onResponse(Response<safeNumData> response, Retrofit retrofit) {
                MyLogs.v("call_safe data : " + response.body().getData().getVirtual_number());
                MyProgressDialog.disMissDialog();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + response.body().getData().getVirtual_number()));
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable t) {
                MyProgressDialog.disMissDialog();
            }
        });
    }

    void put_solved() {
        MyProgressDialog.showDialog(context, "잠시만 기다려주세요..");
        Call<Register_CallBackItem> callback = service.solved_register(item.getData().getP_id(), StaticData.initPreference(context).getValue(RbPreference.USER_NUM, ""));
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                MyProgressDialog.disMissDialog();
                if (item.getData().getP_is_solved().equals("n")) {
                    solved_tv.setText("완료");
                    solved_tv.setBackgroundResource(R.color.solved_color);
                    item.getData().setP_is_solved("y");
                    solved_btn.setText("미해결상태로 변환하기");
                } else {
                    solved_tv.setText("미해결");
                    solved_tv.setBackgroundResource(R.color.un_solved_color);
                    item.getData().setP_is_solved("n");
                    solved_btn.setText("해결상태로 변환하기");
                }
                Toast.makeText(context, "완료되었습니다.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t) {
                MyProgressDialog.disMissDialog();
            }
        });
    }

    void setTextViewData() {
        if(item.getData().getP_is_solved().equals("n")) {
            solved_tv.setText("미해결");
            solved_tv.setBackgroundResource(R.color.un_solved_color);
            solved_btn.setText("해결상태로 변환하기");
        } else {
            solved_tv.setText("완료");
            solved_tv.setBackgroundResource(R.color.solved_color);
            solved_btn.setText("미해결상태로 변환하기");
        }
        title_tv.setText(item.getData().getP_title());
        name_tv.setText("이름 : " + item.getData().getP_name());
        happenDate_tv.setText("발생 일시 : " + item.getData().getP_happen_date());
        age_tv.setText("현재 나이 : " + item.getData().getP_age());
        feature_tv.setText("특징 : " + item.getData().getP_feature());
        location_detail_tv.setText("지역 : " + item.getData().getP_location_detail_text());
        money_tv.setText("사례금 : " + item.getData().getP_money() + " 만원");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }

}
