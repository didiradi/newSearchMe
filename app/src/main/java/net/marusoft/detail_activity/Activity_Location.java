package net.marusoft.detail_activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;
import net.marusoft.common.BaseActionbarActivity;
import net.marusoft.common.Post_Detail_Item;
import net.marusoft.search_me.R;

import butterknife.ButterKnife;
import util.MyLogs;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-04.
 */
public class Activity_Location extends BaseActionbarActivity {
    private Context context;
    private Post_Detail_Item item;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        context = this;

        Initialize();

        item = (Post_Detail_Item) getIntent().getExtras().getSerializable("post_detail_item");
        final MapView mapView = new MapView(context);
        mapView.setDaumMapApiKey(StaticData.DAUMMAPKEY);

        ViewGroup mapViewContainer = (ViewGroup) findViewById(R.id.map_view);

        if(item.getData().getP_location_latitude().equals("") || item.getData().getP_location_longitude().equals("")) {
            Toast.makeText(context, "위치 부정확", Toast.LENGTH_SHORT).show();
        } else {
            mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(Float.parseFloat(item.getData().getP_location_latitude()), Float.parseFloat(item.getData().getP_location_longitude())), true);
            MapPOIItem marker = new MapPOIItem();
            marker.setItemName(item.getData().getP_title());
            marker.setTag(0);
            marker.setMapPoint(MapPoint.mapPointWithGeoCoord(Float.parseFloat(item.getData().getP_location_latitude()), Float.parseFloat(item.getData().getP_location_longitude())));
            marker.setMarkerType(MapPOIItem.MarkerType.BluePin); // 기본으로 제공하는 BluePin 마커 모양.
            marker.setSelectedMarkerType(MapPOIItem.MarkerType.RedPin); // 마커를 클릭했을때, 기본으로 제공하는 RedPin 마커 모양.

            mapView.addPOIItem(marker);
        }
        MyLogs.v("latlong data : " + item.getData().getP_location_latitude() + " : " + item.getData().getP_location_longitude());

        //줌레벨
        mapView.setZoomLevel(1, true);

        mapViewContainer.addView(mapView);
    }

    void Initialize() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }
}
