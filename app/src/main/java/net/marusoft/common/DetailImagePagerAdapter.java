package net.marusoft.common;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.marusoft.search_me.R;

import java.util.ArrayList;

import util.MyLogs;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class DetailImagePagerAdapter extends PagerAdapter {
    Context context;
    LayoutInflater mLayoutInflater;
    ArrayList<String> img_list;
    public DetailImagePagerAdapter(Context context, ArrayList<String> img_list) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.img_list = img_list;
    }

    @Override
    public int getCount() {
        return img_list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager_item, container, false);

        TextView page_tv = (TextView) itemView.findViewById(R.id.page_tv);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        if(img_list.get(0) == null) {
            //imageView.setImageResource(R.drawable.no_image);
        } else {
           // MyLogs.v("img_list : " + StaticData.IMG_URL + img_list.get(position));
            //Glide.with(context).load(img_list.get(position)).into(imageView);
            MyLogs.v("crop : 2" + Uri.parse(img_list.get(position)));
            imageView.setImageURI(Uri.parse(img_list.get(position)));
        }
        page_tv.setText("(" + (position + 1) + " / " + img_list.size() + ")"); //이미지 페이지 번호
//        itemView.setClickable(true);
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, Image_Detail_PagerActivity.class);
//                intent.putStringArrayListExtra("image_list", img_list);
//                intent.putExtra("position", position);
//                context.startActivity(intent);
//            }
//        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
