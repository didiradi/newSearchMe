package net.marusoft.common;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Henen on 2016-02-02.
 */
public class CategoryDB_Sub_Item extends RealmObject implements Serializable{

    private static final long serialVersionUID = 1320L;

    @PrimaryKey
    private int c_id;
    private int c_major;
    private int c_number;
    private String c_name;


    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public int getC_major() {
        return c_major;
    }

    public void setC_major(int c_major) {
        this.c_major = c_major;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public int getC_number() {
        return c_number;
    }

    public void setC_number(int c_number) {
        this.c_number = c_number;
    }
}
