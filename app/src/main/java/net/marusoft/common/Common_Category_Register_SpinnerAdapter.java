package net.marusoft.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.marusoft.search_me.R;

import java.util.ArrayList;

import io.realm.RealmList;


/**
 * Created by SEO on 2016-02-03.
 */
public class Common_Category_Register_SpinnerAdapter extends BaseAdapter {

    private Context context;
   private CommonCategory commonCategory;
    ViewHolder viewHolder;
    int self_register_position = 0;

    public Common_Category_Register_SpinnerAdapter(Context context, CommonCategory commonCategory) {
        this.context = context;
        this.commonCategory = commonCategory;
        self_register_position = setSelf_register_position();
    }


    public int getSelf_register_position(){
        return  self_register_position;
    }

    int setSelf_register_position(){
        int postion = 0;
        ArrayList<Category_Sub_Item> sub =  commonCategory.getSubCategory();
        CategoryDB_Sub_Item sub_item =null;
        for(int i = 0 ; i< sub.size();i++){
            if(sub.get(i).getC_name().equals("직접등록")){
                postion = i;
                break;
            }
        }
        return  postion;
    }

    public void addCategory(String name){
        Category_Sub_Item sub_item =    commonCategory.getSubCategory().get(self_register_position);
        Category_Sub_Item sub_item1 = new Category_Sub_Item(commonCategory.getSubCategoryItem(self_register_position));
            sub_item1.setC_id(sub_item1.getC_id());
             sub_item1.setC_number(sub_item1.getC_number());
            sub_item1.setC_name(name);
            sub_item1.setC_major(sub_item.getC_major());
            commonCategory.getSubCategory().add(sub_item1);
    }

    @Override
    public int getCount() {
        return commonCategory.sub_count();
    }

    @Override
    public Object getItem(int position) {
        return commonCategory.getSubCategoryItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
           convertView  = LayoutInflater.from(context).inflate(R.layout.spinner_item,null);
            viewHolder = new ViewHolder();
            viewHolder.titleTextView = (TextView)convertView.findViewById(R.id.title_textview);
            convertView.setTag(viewHolder);

        }else{
           viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.titleTextView.setText(commonCategory.getSubCategoryItem(position).getC_name());
        return convertView;
    }



    class ViewHolder{
      public TextView titleTextView;
    }



}
