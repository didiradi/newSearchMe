package net.marusoft.common;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SEO on 2016-02-02.
 */
@SuppressWarnings("serial")
public class CommonCategory implements Serializable {


   private Category_Major_Item category_major_item;

    public CommonCategory(CategoryDB_Major_Item categoryDB_major_item) {
        this.category_major_item = new Category_Major_Item(categoryDB_major_item);
    }


    public ArrayList<Category_Sub_Item> getSubCategory(){
        return this.category_major_item.getSub();
    }


    public Category_Sub_Item getSubCategoryItem(int position){
        Category_Sub_Item result_item = null;
        result_item =  this.category_major_item.getSub().get(position);
        return result_item;
    }


    public  int getC_id(String title){
        int result_id = -1;
        for(int i  = 0 ;  i< this.category_major_item.getSub().size();i++){
            if( this.category_major_item.getSub().get(i).getC_name().equals(title)){
                result_id =  this.category_major_item.getSub().get(i).getC_id();
                break;
            }
        }
        return result_id;
    }


    public  String getC_name(int id){
        String result_title = "";
        for(int i  = 0 ;  i< this.category_major_item.getSub().size();i++){
            if( this.category_major_item.getSub().get(i).getC_id() == id){
                result_title =  this.category_major_item.getSub().get(i).getC_name();
                break;
            }
        }
        return result_title;
    }




    public  String [] get_name_array(){
        String [] name_array = new String [ this.category_major_item.getSub().size()];
        for(int i  = 0 ;  i< this.category_major_item.getSub().size();i++){
            name_array[i] =  this.category_major_item.getSub().get(i).getC_name();

        }
        return name_array;
    }


    public int sub_count(){
        return  this.category_major_item.getSub().size();
    }


}
