package net.marusoft.common;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import net.marusoft.search_me.R;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */

public final class ImageAdapterFragment extends Fragment {
    private ArrayList<String> image_list;
    int position;
    PhotoView m_imageView;
    private Context context;
    public ImageAdapterFragment(Context context, ArrayList<String> image_list, int position) {
        this.image_list = image_list;
        this.position = position;
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = (View) inflater.inflate(R.layout.image_zoom_page, null);
        m_imageView = (PhotoView)root.findViewById(R.id.slider_image);
        if(image_list.get(0) == null) {

        } else {
            Glide.with(context).load(StaticData.IMG_URL + image_list.get(position)).into(m_imageView);
        }

        return root;
    }
}
