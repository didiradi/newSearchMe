package net.marusoft.common;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.marusoft.detail_activity.Activity_LostPerson_Detail;
import net.marusoft.detail_activity.Activity_Person_Detail;
import net.marusoft.detail_activity.Activity_Pet_Detail;
import net.marusoft.detail_activity.Activity_Tip_Detail;
import net.marusoft.detail_activity.Activity_Wanted_Detail;
import net.marusoft.detail_activity.Activity_Witness_Detail;
import net.marusoft.detail_activity.LostAndFound_Detail_Activity;
import net.marusoft.search_me.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import util.MyLogs;

/**
 * Created by MARUSOFT-Henen on 2016-01-29.
 */
public class CommonSearchMe_List_Adapter extends RecyclerView.Adapter<CommonSearchMe_List_Adapter.ViewHolder>{
    private ArrayList<Post_Detail_Item_Data> items;
    Context context;
    int solvedColor;
    int unSolvedColor;
    boolean category_textview_visible = false;
    boolean categoty_direct_bool = false;
    public CommonSearchMe_List_Adapter(Context context, ArrayList<Post_Detail_Item_Data> items) {
        this.context=context;
        this.items=items;
        category_textview_visible = false;
        if(context == null) {

        } else {
            solvedColor =  context.getResources().getColor(R.color.solved_color);
            unSolvedColor =  context.getResources().getColor(R.color.un_solved_color);
        }

        MyLogs.v("test");
    }


    public CommonSearchMe_List_Adapter(Context context, ArrayList<Post_Detail_Item_Data> items,boolean category_textview_visible) {
        this.context=context;
        this.items=items;
        if(context == null) {

        } else {
            solvedColor =  context.getResources().getColor(R.color.solved_color);
            unSolvedColor =  context.getResources().getColor(R.color.un_solved_color);
        }

        this.category_textview_visible = category_textview_visible;
        MyLogs.v("test");
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.common_searchme_list_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        /*if(category_textview_visible==true){
            holder.category_textview.setVisibility(View.VISIBLE);
        }else{
            holder.category_textview.setVisibility(View.GONE);
        }*/
        holder.category_textview.setVisibility(View.GONE);
        holder.item_textview1.setVisibility(View.VISIBLE);
        holder.item_textview2.setVisibility(View.VISIBLE);
        holder.item_textview3.setVisibility(View.VISIBLE);
        holder.item_textview4.setVisibility(View.VISIBLE);
        holder.item_textview5.setVisibility(View.VISIBLE);
        holder.item_textview6.setVisibility(View.VISIBLE);



        final Post_Detail_Item_Data item = items.get(position);
        MyLogs.v("c_data" + item.toString());


        //holder.category_textview.setText(item.getP_category_name());
        if(item.getP_category_id().equals("17") || item.getP_category_id().equals("21") || item.getP_category_id().equals("27") || item.getP_category_id().equals("30") || item.getP_category_id().equals("34") || item.getP_category_id().equals("36")) {
            holder.category_tv.setVisibility(View.VISIBLE);
            categoty_direct_bool = true;
        } else {
            categoty_direct_bool = false;
        }


        if(item.getThum_photo()==null){
            Glide.with(context).load(R.drawable.search_me_box).into(holder.thum_imageview);
        }else{
            Glide.with(context).load(SearchMe_APIService.THUMNAIL_HOST_URL+item.getThum_photo()).into(holder.thum_imageview);
        }

        if(item.getMajor_category()==1){//현상범

            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundColor(unSolvedColor);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundColor(solvedColor);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_Wanted_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id",item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_Wanted_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id",item.getP_id());
                        context.startActivity(intent);
                    }
                }
            });


            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);
        }
        else if(item.getMajor_category()==2){ //반려동물

            holder.title_textview.setText(item.getP_title());

           /* if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }*/

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundResource(R.color.un_solved_color);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundResource(R.color.solved_color);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_Pet_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_Pet_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id",item.getP_id());
                        context.startActivity(intent);
                    }

                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);
        }
        else
        if(item.getMajor_category()==3){ //분실물

            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundResource(R.color.un_solved_color);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundResource(R.color.solved_color);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, LostAndFound_Detail_Activity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, LostAndFound_Detail_Activity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id",item.getP_id());
                        context.startActivity(intent);
                    }

                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);
        }

        else if(item.getMajor_category()==4){ //실종
            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundColor(unSolvedColor);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundColor(solvedColor);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_LostPerson_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_LostPerson_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        context.startActivity(intent);
                    }
                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);

        }

        else if(item.getMajor_category()==5){ //제보를 받습니다.
            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundColor(unSolvedColor);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundColor(solvedColor);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_Tip_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_Tip_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        context.startActivity(intent);
                    }
                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);


        }
        else if(item.getMajor_category()==6){ //목격자
            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundColor(unSolvedColor);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundColor(solvedColor);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_Witness_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_Witness_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        context.startActivity(intent);
                    }
                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);


        }

        else if(item.getMajor_category()==7){ //사람
            holder.title_textview.setText(item.getP_title());

            if(categoty_direct_bool) {
                holder.category_tv.setText("카테고리 : " + item.getP_category_name());
            }

            if(item.getP_is_solved().equals("n")){
                holder.solved_textview.setText("미해결");
                holder.solved_textview.setBackgroundColor(unSolvedColor);
            }else{
                holder.solved_textview.setText("완료");
                holder.solved_textview.setBackgroundColor(solvedColor);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(category_textview_visible) {
                        Intent intent = new Intent(context, Activity_Person_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        intent.putExtra("my_upload", true);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, Activity_Person_Detail.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("p_id", item.getP_id());
                        context.startActivity(intent);
                    }
                }
            });

            holder.item_textview1.setText(item.getP_write_date());
            holder.item_textview2.setText(item.getP_location_text());
            holder.item_textview3.setText(item.getP_location_detail_text());
            holder.item_textview4.setText("사례금 : "+ item.getP_money() + " 만원");
            holder.item_textview5.setVisibility(View.GONE);
            holder.item_textview6.setVisibility(View.GONE);

        }





    }


    @Override
    public int getItemCount() {
        return this.items.size();
    }
    public void addItems(List<Post_Detail_Item_Data> mitem) {
        items.addAll(mitem);
        notifyItemInserted(items.size() - 1);

    }


    public class ViewHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.category_textview)
        TextView category_textview;
        @Bind(R.id.thumb_imageview)
        ImageView thum_imageview;
        @Bind(R.id.title_textview)
        TextView title_textview;
        @Bind(R.id.solved_textview)
        TextView solved_textview;
        @Bind(R.id.item_textview1)
        TextView item_textview1;
        @Bind(R.id.item_textview2)
        TextView item_textview2;
        @Bind(R.id.item_textview3)
        TextView item_textview3;
        @Bind(R.id.item_textview4)
        TextView item_textview4;
        @Bind(R.id.item_textview5)
        TextView item_textview5;
        @Bind(R.id.item_textview6)
        TextView item_textview6;
        @Bind(R.id.category_tv)
        TextView category_tv;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
