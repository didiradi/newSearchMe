package net.marusoft.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import net.marusoft.search_me.R;

import java.util.ArrayList;

import uk.co.senab.photoview.sample.HackyViewPager;
import util.MyLogs;

/**
 * Created by choi on 2015-07-08.
 */
public class Image_Detail_PagerActivity extends ActionBarActivity {
    HackyViewPager mPager;
    ImageZoomAdapter mAdapter;
    int position;
    ArrayList<String> image_list = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_pager);

        Intent intent = getIntent();
        image_list = intent.getStringArrayListExtra("image_list");
        position = intent.getIntExtra("position", 0);

        mAdapter = new ImageZoomAdapter(this, getSupportFragmentManager(), image_list);
        MyLogs.v("image_lst : " + image_list.get(position).toString());
        mPager = (HackyViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(position);
    }

}
