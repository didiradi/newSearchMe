package net.marusoft.common;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created Henen on 2016-01-28.
 */
public class BaseActivity extends AppCompatActivity {
    public Retrofit retrofit;
    public SearchMe_APIService service;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         retrofit = new Retrofit.Builder()
                .baseUrl(SearchMe_APIService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

         service = retrofit.create(SearchMe_APIService.class);
        permissionAllow();



    }

    public void permissionAllow(){
        PermissionGen.with(BaseActivity.this)
                .addRequestCode(100)
                .permissions(
                        android.Manifest.permission.INTERNET,
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_NETWORK_STATE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.RECEIVE_SMS
                ).request();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }


    @PermissionSuccess(requestCode = 100)
    public void doSomething(){
    }
    @PermissionFail(requestCode = 100)
    public void doFailSomething(){
        AlertDialog.Builder ab = new AlertDialog.Builder(BaseActivity.this,android.R.style.Theme_Material_Light_Dialog)
                .setTitle("써치미")
                .setMessage("권한을 설정하지 않으시면 앱을 이용하실수 없습니다.")
                .setPositiveButton("앱 종료", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("설정하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionAllow();
                    }
                }).setCancelable(false);

        ab.show();
    }
}
