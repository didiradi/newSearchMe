package net.marusoft.common;

/**
 * Created by MARUSOFT-CHOI on 2016-02-04.
 */
public class Favorite_Item {
    public Meta meta;
    public Data data;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Meta {
        String code;
        String state;
        String msg;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "Meta{" +
                    "code='" + code + '\'' +
                    ", state='" + state + '\'' +
                    ", msg='" + msg + '\'' +
                    '}';
        }
    }

    public class Data {
        String favorite_id;

        public String getFavorite_id() {
            return favorite_id;
        }

        public void setFavorite_id(String favorite_id) {
            this.favorite_id = favorite_id;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "favorite_id='" + favorite_id + '\'' +
                    '}';
        }
    }
}
