package net.marusoft.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by MARUSOFT-CHOI and Henen on 2016-02-03.
 */
@SuppressWarnings("serial")
public class Post_Detail_Item_Data implements Serializable{
        String p_id;
        String p_user_id;
        String p_category_id;
        String p_category_name;
        String p_number;
        String p_title;
        String p_name;
        String p_feature;
        String p_contents;
        String p_feature_clothes;
        String p_money;
        String p_location_id;
        String p_location_text;
        String p_location_detail_text;
        String p_location_latitude;
        String p_location_longitude;
        String p_time;
        String thum_photo;
        String p_responsibility;
        String p_lost_property;
        String p_happen_date;
        String p_age;
        String p_is_solved;
        String p_write_date;
        String p_isfavorite;
        String u_name;
        String virtual_number;
        int major_category;


    public int getMajor_category() {
        return major_category;
    }

    public void setMajor_category(int major_category) {
        this.major_category = major_category;
    }

    public String getP_id() {
            return p_id;
        }

        public void setP_id(String p_id) {
            this.p_id = p_id;
        }

        public String getP_user_id() {
            return p_user_id;
        }

        public void setP_user_id(String p_user_id) {
            this.p_user_id = p_user_id;
        }

        public String getP_category_id() {
            return p_category_id;
        }

        public void setP_category_id(String p_category_id) {
            this.p_category_id = p_category_id;
        }

        public String getP_category_name() {
            return p_category_name;
        }

        public void setP_category_name(String p_category_name) {
            this.p_category_name = p_category_name;
        }


    public String getP_location_id() {
        return p_location_id;
    }

    public void setP_location_id(String p_location_id) {
        this.p_location_id = p_location_id;
    }

    public String getP_number() {
            return p_number;
        }

        public void setP_number(String p_number) {
            this.p_number = p_number;
        }

        public String getP_title() {
            return p_title;
        }

        public void setP_title(String p_title) {
            this.p_title = p_title;
        }

        public String getP_name() {
            return p_name;
        }

        public void setP_name(String p_name) {
            this.p_name = p_name;
        }

        public String getP_feature() {
            return p_feature;
        }

        public void setP_feature(String p_feature) {
            this.p_feature = p_feature;
        }

        public String getP_contents() {
            return p_contents;
        }

        public void setP_contents(String p_contents) {
            this.p_contents = p_contents;
        }

        public String getP_feature_clothes() {
            return p_feature_clothes;
        }

        public void setP_feature_clothes(String p_feature_clothes) {
            this.p_feature_clothes = p_feature_clothes;
        }

        public String getP_money() {
            return p_money;
        }

        public void setP_money(String p_money) {
            this.p_money = p_money;
        }

        public String getP_location_text() {
            return p_location_text;
        }

        public void setP_location_text(String p_location_text) {
            this.p_location_text = p_location_text;
        }

        public String getP_location_detail_text() {
        return p_location_detail_text;
    }

        public void setP_location_detail_text(String p_location_detail_text) {
        this.p_location_detail_text = p_location_detail_text;
         }


    public String getP_location_latitude() {
            return p_location_latitude;
        }

        public void setP_location_latitude(String p_location_latitude) {
            this.p_location_latitude = p_location_latitude;
        }

        public String getP_location_longitude() {
            return p_location_longitude;
        }

        public void setP_location_longitude(String p_location_longitude) {
            this.p_location_longitude = p_location_longitude;
        }

        public String getP_time() {
            return p_time;
        }

        public void setP_time(String p_time) {
            this.p_time = p_time;
        }

        public String getP_responsibility() {
            return p_responsibility;
        }

        public void setP_responsibility(String p_responsibility) {
            this.p_responsibility = p_responsibility;
        }

        public String getP_lost_property() {
            return p_lost_property;
        }

        public void setP_lost_property(String p_lost_property) {
            this.p_lost_property = p_lost_property;
        }

        public String getP_happen_date() {
            return p_happen_date;
        }

        public void setP_happen_date(String p_happen_date) {
            this.p_happen_date = p_happen_date;
        }

        public String getP_age() {
            return p_age;
        }

        public void setP_age(String p_age) {
            this.p_age = p_age;
        }

        public String getP_is_solved() {
            return p_is_solved;
        }

        public void setP_is_solved(String p_is_solved) {
            this.p_is_solved = p_is_solved;
        }

        public String getP_write_date() {
            return p_write_date;
        }

        public void setP_write_date(String p_write_date) {
            this.p_write_date = p_write_date;
        }

    public String getP_isfavorite() {
        return p_isfavorite;
    }

    public void setP_isfavorite(String p_isfavorite) {
        this.p_isfavorite = p_isfavorite;
    }

    public ArrayList<Post_Detail_Item_Photo> photo;

    public ArrayList<Post_Detail_Item_Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(ArrayList<Post_Detail_Item_Photo> photo) {
        this.photo = photo;
    }


    public String getThum_photo() {
        return thum_photo;
    }

    public void setThum_photo(String thum_photo) {
        this.thum_photo = thum_photo;
    }

    public String getVirtual_number() {
        return virtual_number;
    }

    public void setVirtual_number(String virtual_number) {
        this.virtual_number = virtual_number;
    }


    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    @Override
    public String toString() {
        return "Post_Detail_Item_Data{" +
                "p_id='" + p_id + '\'' +
                ", p_user_id='" + p_user_id + '\'' +
                ", p_category_id='" + p_category_id + '\'' +
                ", p_category_name='" + p_category_name + '\'' +
                ", p_number='" + p_number + '\'' +
                ", p_title='" + p_title + '\'' +
                ", p_name='" + p_name + '\'' +
                ", p_feature='" + p_feature + '\'' +
                ", p_contents='" + p_contents + '\'' +
                ", p_feature_clothes='" + p_feature_clothes + '\'' +
                ", p_money='" + p_money + '\'' +
                ", p_location_id='" + p_location_id + '\'' +
                ", p_location_text='" + p_location_text + '\'' +
                ", p_location_detail_text='" + p_location_detail_text + '\'' +
                ", p_location_latitude='" + p_location_latitude + '\'' +
                ", p_location_longitude='" + p_location_longitude + '\'' +
                ", thum_photo='" + thum_photo + '\'' +
                ", p_responsibility='" + p_responsibility + '\'' +
                ", p_lost_property='" + p_lost_property + '\'' +
                ", p_happen_date='" + p_happen_date + '\'' +
                ", p_age='" + p_age + '\'' +
                ", p_is_solved='" + p_is_solved + '\'' +
                ", p_write_date='" + p_write_date + '\'' +
                ", u_name='" + u_name + '\'' +
                ", p_isfavorite='" + p_isfavorite + '\'' +
                ", u_name='" + u_name + '\'' +
                ", virtual_number='" + virtual_number + '\'' +
                ", major_category=" + major_category +
                ", photo=" + photo +
                '}';
    }
}
