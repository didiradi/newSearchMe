package net.marusoft.common;

import java.util.List;

/**
 * Created by Henen on 2016-02-03.
 */
public class Common_SearchMeItem_CallBack {
    private List<Post_Detail_Item_Data> data;

    private Meta meta;

    public List<Post_Detail_Item_Data> getData ()
    {
        return data;
    }

    public void setData (List<Post_Detail_Item_Data> data)
    {
        this.data = data;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }

    public class Meta
    {
        private String state;

        private String code;

        private String msg;

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getMsg ()
        {
            return msg;
        }

        public void setMsg (String msg)
        {
            this.msg = msg;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", code = "+code+", msg = "+msg+"]";
        }
    }


}
