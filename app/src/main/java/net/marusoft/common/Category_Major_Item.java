package net.marusoft.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SEO on 2016-02-15.
 */



public class Category_Major_Item implements Serializable{


    private int c_id;
    private int c_major;
    private int c_number;
    private String c_name;
    private ArrayList<Category_Sub_Item> sub;

    public Category_Major_Item(CategoryDB_Major_Item categoryDB_major_item){
            this.c_id = categoryDB_major_item.getC_id();
            this.c_major  = categoryDB_major_item.getC_major();
            this.c_name = categoryDB_major_item.getC_name();
            this.c_number = categoryDB_major_item.getC_number();
            sub = new ArrayList<>();
            for(int i =0 ; i<categoryDB_major_item.getSub().size();i++){
                sub.add(new Category_Sub_Item(categoryDB_major_item.getSub().get(i)));
            }
    }


    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public int getC_major() {
        return c_major;
    }

    public void setC_major(int c_major) {
        this.c_major = c_major;
    }

    public int getC_number() {
        return c_number;
    }

    public void setC_number(int c_number) {
        this.c_number = c_number;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public ArrayList<Category_Sub_Item> getSub() {
        return sub;
    }

    public void setSub(ArrayList<Category_Sub_Item> sub) {
        this.sub = sub;
    }


    @Override
    public String toString() {
        return "Category_Major_Item{" +
                "c_id=" + c_id +
                ", c_major=" + c_major +
                ", c_number=" + c_number +
                ", c_name='" + c_name + '\'' +
                ", sub=" + sub +
                '}';
    }
}
