package net.marusoft.common;

/**
 * Created by SEO on 2016-02-02.
 */
public class Register_CallBackItem
{
    public  boolean isResponseSuccess(){
        if(this.meta==null){
            return false;
        }else {
            if (this.meta.getState().equals("OK")) {
                return true;
            } else {
                return false;
            }
        }
    }

    private Data data;

    private Meta meta;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }

    public class Data
    {
        private int post_id;

        private int order;

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public int getPost_id ()
        {
            return post_id;
        }

        public void setPost_id (int post_id)
        {
            this.post_id = post_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [post_id = "+post_id+"order = "+order+"]";
        }
    }
    public class Meta
    {
        private String state;

        private String code;

        private String msg;




        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getMsg ()
        {
            return msg;
        }

        public void setMsg (String msg)
        {
            this.msg = msg;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", code = "+code+", msg = "+msg+"]";
        }
    }

}

