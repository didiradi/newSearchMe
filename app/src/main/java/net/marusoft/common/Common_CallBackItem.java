package net.marusoft.common;

/**
 * Created by SEO on 2016-02-02.
 */

public class Common_CallBackItem
{

    public  boolean isResponseSuccess() {
        if (this.meta == null) {
            return false;
        } else {
            if (this.meta.getState().equals("OK")) {
                return true;
            } else {
                return false;
            }
        }
    }


    private Meta meta;

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [meta = "+meta+"]";
    }
    public class Meta
    {
        private String state;

        private String code;

        private String msg;

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getMsg ()
        {
            return msg;
        }

        public void setMsg (String msg)
        {
            this.msg = msg;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", code = "+code+", msg = "+msg+"]";
        }
    }
}
