package net.marusoft.common;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by MARUSOFT-CHOI on 2016-02-04.
 */
public class BaseActionbarActivity extends ActionBarActivity{
    public Retrofit retrofit;
    public SearchMe_APIService service;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrofit = new Retrofit.Builder()
                .baseUrl(SearchMe_APIService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(SearchMe_APIService.class);
    }
}
