package net.marusoft.common;

import com.squareup.okhttp.RequestBody;

import net.marusoft.search_me.FavoriteListCallBack;

import main_register.Register_Data;
import main_register.User_Data;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by CHOI on 2016-01-29.
 */
public interface SearchMe_APIService {
    public static final String API_URL = "http://serchme.cafe24.com/api/";
    public static final String THUMNAIL_HOST_URL = "http://serchme.cafe24.com/thum/";
    public static final String IMAGE_HOST_URL = "http://serchme.cafe24.com/img/";
    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> lost_and_found_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("lost_property") String lost_property,
            @Field("feature") String feature,
            @Field("contents") String contents,
            @Field("happen_date") String happen_date,
            @Field("location_id") String location_id,
            @Field("location_text") String location_text,
            @Field("location_detail") String location_detail_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> witness_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("name") String name,
            @Field("location_text") String location_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("time") String time,
            @Field("contents") String contents,
            @Field("location_detail") String location_detail_text,
            @Field("location_id") String location_id);


    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> tip_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("responsibility") String responsibility,
            @Field("contents") String contents,
            @Field("location_id") String location_id,
            @Field("location_text") String location_text,
            @Field("location_detail") String location_detail_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> pet_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("lost_property") String lost_property,
            @Field("feature") String feature,
            @Field("contents") String contents,
            @Field("happen_date") String happen_date,
            @Field("location_id") String location_id,
            @Field("location_text") String location_text,
            @Field("location_detail") String location_detail_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);


    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> person_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("name") String name,
            @Field("feature") String feature,
            @Field("location_text") String location_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("location_detail") String location_detail_text,
            @Field("location_id") String location_id);


    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> wanted_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("name") String name,
            @Field("feature") String feature,
            @Field("location_text") String location_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("location_detail") String location_detail_text,
            @Field("location_id") String location_id);


    @FormUrlEncoded
    @POST("post_register.php")
    Call<Register_CallBackItem> lostperson_post_register(
            @Field("category_id") int category_id,
            @Field("category_name") String category_name,
            @Field("category_number") int category_number,
            @Field("user_id") String user_id,
            @Field("title") String title,
            @Field("money") String money,
            @Field("name") String name,
            @Field("age") String age,
            @Field("feature") String feature,
            @Field("happen_date") String happen_date,
            @Field("location_id") String location_id,
            @Field("location_text") String location_text,
            @Field("location_detail") String location_detail_text,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);




    @Multipart
    @POST("post_photo_register.php")
    Call<Common_CallBackItem> post_photo_register(
            @Part("image\"; filename=\"image.jpg") RequestBody imagefile,
            @Part("order") RequestBody order_id,
            @Part("post_id") RequestBody post_id);




    @POST("category_list.php")
    Call<CategoryDB_CallBackItem> getCategory();


    @FormUrlEncoded
    @POST("post_list.php")
    Call<Common_SearchMeItem_CallBack> post_list(@Field("category_id") int category_id, @Field("start_index") int start_index, @Field("count") int count);


    @FormUrlEncoded
    @POST("user_post_list.php")
    Call<Common_SearchMeItem_CallBack> user_post_list(@Field("user_id") String user_id, @Field("start_index") int start_index, @Field("count") int count);


    @FormUrlEncoded
    @POST("favorite_list.php")
    Call<FavoriteListCallBack> favorite_list(@Field("user_id") String user_id, @Field("start_index") int start_index, @Field("count") int count);



    @FormUrlEncoded
    @POST("user_name_check.php")
    Call<Register_CallBackItem> user_name_check(
            @Field("name") String name);

    @FormUrlEncoded
    @POST("phone_duplication_check.php")
    Call<Register_CallBackItem> phone_duplication_check(
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("user_register.php")
    Call<Register_Data> user_register(
            @Field("phone") String phone,
            @Field("name") String name,
            @Field("location_name") String location_name,
            @Field("location_id") String location_id,
            @Field("reg_id") String reg_id,
            @Field("device_id") String device_id,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("push_check.php")
    Call<Register_CallBackItem> push_check(
            @Field("user_id") String user_id,
            @Field("category1") String category1,
            @Field("category2") String category2,
            @Field("category3") String category3,
            @Field("category4") String category4,
            @Field("category5") String category5,
            @Field("category6") String category6,
            @Field("category7") String category7);

    @FormUrlEncoded
    @POST("phone_check.php")
    Call<Register_CallBackItem> phone_check(
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("phone_auth_check.php")
    Call<Register_CallBackItem> phone_auth_check(
            @Field("phone") String phone,
            @Field("auth") String auth);

    @FormUrlEncoded
    @POST("post_detail.php")
    Call<Post_Detail_Item> post_detail(
            @Field("user_id") String user_id,
            @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("favorite_register.php")
    Call<Favorite_Item> favorite_register(
            @Field("user_id") String user_id,
            @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("favorite_delete.php")
    Call<Register_CallBackItem> favorite_delete(
            @Field("user_id") String user_id,
            @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("get_user_data.php")
    Call<User_Data> get_user_data(
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("post_phone_register.php")
    Call<safeNumData> post_phone_register(
            @Field("post_id") String post_id,
            @Field("post_user_id") String post_user_id);

    @FormUrlEncoded
    @POST("user_location_register.php")
    Call<Register_CallBackItem> user_location_register(
            @Field("user_id") String user_id,
            @Field("location_id") String location_id,
            @Field("location_name") String location_name,
            @Field("location_fixed") String location_fixed,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("solved_register.php")
    Call<Register_CallBackItem> solved_register(
            @Field("p_id") String p_id,
            @Field("user_id") String user_id);
}
