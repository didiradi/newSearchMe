package net.marusoft.common;

/**
 * Created by Henen on 2016-02-02.
 */
public class CategoryDB_CallBackItem {
    private Data[] data;

    private Meta meta;

    public  boolean isResponseSuccess() {
        if (this.meta == null) {
            return false;
        } else {
            if (this.meta.getState().equals("OK")) {
                return true;
            } else {
                return false;
            }
        }
    }



    public Data[] getData ()
    {
        return data;
    }

    public void setData (Data[] data)
    {
        this.data = data;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }
    public class Data
    {
        private Sub[] sub;

        private int c_major;

        private int c_id;

        private int c_number;

        private String c_name;

        public Sub[] getSub ()
        {
            return sub;
        }

        public void setSub (Sub[] sub)
        {
            this.sub = sub;
        }

        public int getC_major ()
        {
            return c_major;
        }

        public void setC_major (int c_major)
        {
            this.c_major = c_major;
        }

        public int getC_id ()
        {
            return c_id;
        }

        public void setC_id (int c_id)
        {
            this.c_id = c_id;
        }

        public int getC_number ()
        {
            return c_number;
        }

        public void setC_number (int c_number)
        {
            this.c_number = c_number;
        }

        public String getC_name ()
        {
            return c_name;
        }

        public void setC_name (String c_name)
        {
            this.c_name = c_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [sub = "+sub+", c_major = "+c_major+", c_id = "+c_id+", c_number = "+c_number+", c_name = "+c_name+"]";
        }
    }
    public class Sub
    {
        private int c_major;

        private int c_id;

        private int c_number;

        private String c_name;

        public int getC_major ()
        {
            return c_major;
        }

        public void setC_major (int c_major)
        {
            this.c_major = c_major;
        }

        public int getC_id ()
        {
            return c_id;
        }

        public void setC_id (int c_id)
        {
            this.c_id = c_id;
        }

        public int getC_number ()
        {
            return c_number;
        }

        public void setC_number (int c_number)
        {
            this.c_number = c_number;
        }

        public String getC_name ()
        {
            return c_name;
        }

        public void setC_name (String c_name)
        {
            this.c_name = c_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [c_major = "+c_major+", c_id = "+c_id+", c_number = "+c_number+", c_name = "+c_name+"]";
        }
    }
    public class Meta
    {
        private String state;

        private String code;

        private String msg;

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getMsg ()
        {
            return msg;
        }

        public void setMsg (String msg)
        {
            this.msg = msg;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", code = "+code+", msg = "+msg+"]";
        }
    }

}
