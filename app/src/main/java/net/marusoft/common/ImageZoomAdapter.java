package net.marusoft.common;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class ImageZoomAdapter extends FragmentPagerAdapter{
    ArrayList<String> image_list;
    private Context context;
    public ImageZoomAdapter(Context context, FragmentManager fm, ArrayList<String> image_list) {
        super(fm);
        this.image_list = image_list;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return new ImageAdapterFragment(context, image_list, position);
    }

    @Override
    public int getCount() {
        return image_list.size();
    }
}
