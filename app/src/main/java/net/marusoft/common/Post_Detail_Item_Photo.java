package net.marusoft.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by MARUSOFT-CHOI and Henen on 2016-02-03.
 */

public class Post_Detail_Item_Photo implements Serializable{

        String pp_id;
        String pp_post_id;
        String pp_img_name;
        String pp_thum_name;
        String pp_img_order;
        String pp_write_date;

        public String getPp_id() {
            return pp_id;
        }

        public void setPp_id(String pp_id) {
            this.pp_id = pp_id;
        }

        public String getPp_post_id() {
            return pp_post_id;
        }

        public void setPp_post_id(String pp_post_id) {
            this.pp_post_id = pp_post_id;
        }

        public String getPp_img_name() {
            return pp_img_name;
        }

        public void setPp_img_name(String pp_img_name) {
            this.pp_img_name = pp_img_name;
        }

        public String getPp_thum_name() {
            return pp_thum_name;
        }

        public void setPp_thum_name(String pp_thum_name) {
            this.pp_thum_name = pp_thum_name;
        }

        public String getPp_img_order() {
            return pp_img_order;
        }

        public void setPp_img_order(String pp_img_order) {
            this.pp_img_order = pp_img_order;
        }

        public String getPp_write_date() {
            return pp_write_date;
        }

        public void setPp_write_date(String pp_write_date) {
            this.pp_write_date = pp_write_date;
        }

        @Override
        public String toString() {
            return "Photo{" +
                    "pp_id='" + pp_id + '\'' +
                    ", pp_post_id='" + pp_post_id + '\'' +
                    ", pp_img_name='" + pp_img_name + '\'' +
                    ", pp_thum_name='" + pp_thum_name + '\'' +
                    ", pp_img_order='" + pp_img_order + '\'' +
                    ", pp_write_date='" + pp_write_date + '\'' +
                    '}';
        }


}
