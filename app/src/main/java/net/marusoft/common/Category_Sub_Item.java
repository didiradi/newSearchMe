package net.marusoft.common;

import java.io.Serializable;

/**
 * Created by Henen on 2016-02-03.
 */
public class Category_Sub_Item implements Serializable{

    private static final long serialVersionUID = 1324L;

    private int c_id;
    private int c_major;
    private int c_number;
    private String c_name;



    public Category_Sub_Item(CategoryDB_Sub_Item categoryDB_sub_item) {
        this.c_id = categoryDB_sub_item.getC_id();
        this.c_major = categoryDB_sub_item.getC_major();
        this.c_number = categoryDB_sub_item.getC_number();
        this.c_name = categoryDB_sub_item.getC_name();
    }


    public Category_Sub_Item(Category_Sub_Item category_sub_item) {
        this.c_id = category_sub_item.getC_id();
        this.c_major = category_sub_item.getC_major();
        this.c_number = category_sub_item.getC_number();
        this.c_name = category_sub_item.getC_name();
    }


    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public int getC_major() {
        return c_major;
    }

    public void setC_major(int c_major) {
        this.c_major = c_major;
    }

    public int getC_number() {
        return c_number;
    }

    public void setC_number(int c_number) {
        this.c_number = c_number;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }
}
