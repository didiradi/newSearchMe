package net.marusoft.common;

import android.content.Context;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;

/**
 * Created by Henen on 2016-02-02.
 */
public class CategoryMananger {

    /*  NUMBER_ID
    *
    *
    *
    *
    */
    public static final int TAG_WANTED= 1000;
    public static final int TAG_PET = 2000;
    public static final int TAG_LOST_AND_FOUND = 3000;
    public static final int TAG_LOST_PERSON = 4000;
    public static final int TAG_TIP = 5000;
    public static final int TAG_WITNESS = 6000; //목격자
    public static final int TAG_PERSON= 7000;//사람을 찾습니다

    public static Retrofit retrofit;
    public static SearchMe_APIService service;

    private static  ArrayList<CommonCategory> categoryArrayList = new ArrayList<>();

    public static void init(){
        if(retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(SearchMe_APIService.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(SearchMe_APIService.class);
        }
    }

    private static void buildDatabase(Context context) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        realm.clear(CategoryDB_Major_Item.class);
        realm.commitTransaction();
        realm.close();
        realm.deleteRealm(realm.getConfiguration());
    }

    public static ArrayList<CommonCategory> getCategoryArrayList(Context context){
        if(categoryArrayList==null) {
            categoryArrayList = new ArrayList<>();
            if (getDBsize(context) == 0) {
                //DB다운로드
                DownLoadDB(context);
            }
            Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            RealmQuery<CategoryDB_Major_Item> query = realm.where(CategoryDB_Major_Item.class);
            RealmResults<CategoryDB_Major_Item> result1 = query.findAll();
            realm.commitTransaction();

            for (int i = 0; i < result1.size(); i++) {
                categoryArrayList.add(new CommonCategory(result1.get(i)));
            }

            realm.close();
        }
            return categoryArrayList;
    }

    public static CommonCategory getCategoryFromNumberId(Context context,int c_number){
        if(categoryArrayList==null || categoryArrayList.size() ==0) {
            categoryArrayList = new ArrayList<>();
            if (getDBsize(context) == 0) {
                //DB다운로드
                DownLoadDB(context);
            }
        }
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        RealmQuery<CategoryDB_Major_Item> query = realm.where(CategoryDB_Major_Item.class);
        RealmResults<CategoryDB_Major_Item> result1 = query.equalTo("c_number", c_number).findAll();
        realm.commitTransaction();
        CommonCategory commonCategory = null;
        commonCategory  = new CommonCategory(result1.get(0));
        realm.close();
        return commonCategory;
    }









    public static int getDBsize(Context context){
        int result = 0;
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        RealmQuery<CategoryDB_Major_Item> query = realm.where(CategoryDB_Major_Item.class);
        RealmResults<CategoryDB_Major_Item> result1 = query.findAll();
        realm.commitTransaction();
        result = result1.size();
        realm.close();
        return  result;
    }

    public static void DownLoadDB(final Context context){
        init();
         Call<CategoryDB_CallBackItem> callback  = service.getCategory();
        callback.enqueue(new Callback<CategoryDB_CallBackItem>() {
            @Override
            public void onResponse(Response<CategoryDB_CallBackItem> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    MyLogs.v(response.body().toString());
                    buildDatabase(context);
                    Realm realm = Realm.getInstance(context);
                            MyLogs.v("" + response.body().getData().length);

                    for(int i = 0 ; i <response.body().getData().length;i++){
                       CategoryDB_CallBackItem.Data data = response.body().getData()[i];
                        RealmList<CategoryDB_Sub_Item>sublist = new RealmList<CategoryDB_Sub_Item>();
                        for(int j = 0 ; j <data.getSub().length;j++){
                            CategoryDB_CallBackItem.Sub sub_data = data.getSub()[j];
                            CategoryDB_Sub_Item st=  new CategoryDB_Sub_Item();
                            st.setC_id(sub_data.getC_id());
                            st.setC_major(sub_data.getC_id());
                            st.setC_name(sub_data.getC_name());
                            st.setC_number(sub_data.getC_number());
                            sublist.add(st);
                        }
                        realm.beginTransaction();
                        CategoryDB_Major_Item categoryDB_major_item = new CategoryDB_Major_Item();
                        categoryDB_major_item.setC_name(data.getC_name());
                        categoryDB_major_item.setC_major(data.getC_major());
                        categoryDB_major_item.setC_number(data.getC_number());
                        categoryDB_major_item.setC_id(data.getC_id());
                        categoryDB_major_item.setSub(sublist);
                        MyLogs.v("" + sublist.size());
                        realm.copyToRealm(categoryDB_major_item);
                        realm.commitTransaction();

                    }
                   realm.close();
                }

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }



    public static CategoryDB_Major_Item findCategoryDB_Major_Item(Context context,int major_id){

                    Realm realm = Realm.getInstance(context);
                    realm.beginTransaction();
                    RealmQuery<CategoryDB_Major_Item> query = realm.where(CategoryDB_Major_Item.class);
                     query.equalTo("c_major", major_id);
                    RealmResults<CategoryDB_Major_Item> result1 = query.findAll();
                    realm.commitTransaction();
                    CategoryDB_Major_Item result_item =  null;
                    result_item = result1.get(0);
                     realm.close();
                    return result_item;
    }





}
