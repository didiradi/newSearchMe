package net.marusoft.common;

import java.io.Serializable;

/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
@SuppressWarnings("serial")
public class Post_Detail_Item implements Serializable {
    public Meta meta;
    public Post_Detail_Item_Data data;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Post_Detail_Item_Data getData() {
        return data;
    }

    public void setData(Post_Detail_Item_Data data) {
        this.data = data;
    }

    @SuppressWarnings("serial")
    public class Meta implements  Serializable{
        String code;
        String state;
        String msg;
    }

}
