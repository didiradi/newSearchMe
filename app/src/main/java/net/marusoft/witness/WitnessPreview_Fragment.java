package net.marusoft.witness;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import net.marusoft.common.BaseFragment;
import net.marusoft.common.Category_Sub_Item;
import net.marusoft.common.Common_CallBackItem;
import net.marusoft.common.DetailImagePagerAdapter;
import net.marusoft.common.Post_Detail_Item_Data;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.search_me.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;

/**
 * Created by  Henen on 2016-02-04.
 */
public class WitnessPreview_Fragment extends BaseFragment implements Witness_register_Activity.onKeyBackPressedListener{
    public static final String TAG = "WITNESS_PREVIEW_FRAGMENT";
    private static final String ARG_POST_DETAIL_ITEM= "ARG_POST_DETAIL_ITEM";
    private static final String ARG_SUB_CATEGORY_ITEM= "ARG_SUB_CATEGORY_ITEM";
    private Post_Detail_Item_Data param_post_detail_item;
    private Category_Sub_Item param_sub_category_item;

    @Bind(R.id.title_tv) TextView title_tv;
    @Bind(R.id.name_tv) TextView name_tv;
    @Bind(R.id.registerName_tv) TextView registerName_tv;
    @Bind(R.id.money_tv) TextView money_tv;
    @Bind(R.id.content_tv) TextView content_tv;
    @Bind(R.id.happenDate_tv) TextView happenDate_tv;
    @Bind(R.id.safenumber_button)  Button safeNumberBtn;



    private Context context;
    private DetailImagePagerAdapter mImagePagerAdapter;
    private AutoScrollViewPager mViewPager;
    private ArrayList<String> img_list;



    public WitnessPreview_Fragment newInstance(Post_Detail_Item_Data post_detail_item_data,Category_Sub_Item category_sub_item){
        WitnessPreview_Fragment witnessPreview_fragment = new WitnessPreview_Fragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POST_DETAIL_ITEM, post_detail_item_data);
        args.putSerializable(ARG_SUB_CATEGORY_ITEM, category_sub_item);
        witnessPreview_fragment.setArguments(args);
        return witnessPreview_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            param_post_detail_item = (Post_Detail_Item_Data)getArguments().getSerializable(ARG_POST_DETAIL_ITEM);
            param_sub_category_item = (Category_Sub_Item)getArguments().getSerializable(ARG_SUB_CATEGORY_ITEM);
        }
    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.witness_preview_fragment,null);
        ButterKnife.bind(this, view);
        MyLogs.v(param_post_detail_item.toString());

        if(param_post_detail_item.getPhoto() != null) {

        img_list = new ArrayList<String>();

        int photo_size =param_post_detail_item.getPhoto().size();
       // MyLogs.v("post_detail_item : " + item.getData().getP_category_id() + " : " + item.getData().getPhoto().get(0).getPp_img_name());
        for(int i=0; i<photo_size; i++) {
            img_list.add(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
        }

        mImagePagerAdapter = new DetailImagePagerAdapter(getActivity(), img_list);
        mViewPager = (AutoScrollViewPager)view.findViewById(R.id.pager);
        mViewPager.setAdapter(mImagePagerAdapter);
        }



        title_tv.setText(param_post_detail_item.getP_title());
        registerName_tv.setText("등록자 : " + param_post_detail_item.getP_name());
        name_tv.setText(param_post_detail_item.getP_name());
        money_tv.setText("사례금 : " + param_post_detail_item.getP_money());
        content_tv.setText(param_post_detail_item.getP_contents());



        happenDate_tv.setText(param_post_detail_item.getP_happen_date());


        safeNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPolicyDialog();


            }
        });


        return view;
    }




    void showPolicyDialog(){
        String policy_text = "본인은 게시물 등록에 있어 아래 써치미의\n 이용약관에 동의하며, 모든 내용을 확인 하였습니다.\n" +
                "1. 해당 목적 외 다른 목적으로 사용하지 않으며, 이로\n" +
                "인한 문제 발생 시 모든 책임은 등록자 본인에게 있음\n" +
                "을 확인 합니다.\n" +
                "2. 등록하는 내용에 허위사실이 없으며, 만약 허위사\n" +
                "실을 등록한 경우 모든 법적 책임을 지겠습니다.\n" +
                "3. 본인의 핸드폰번호를 이용하여 안심번호를 생성하\n" +
                "는 것을 동의하며, 이를 악용하지 않겠습니다.\n" +
                "써치미는 플랫폼 제공사업자로서 당사자간에\n" +
                "발생할 수 있는 일에 대해서는 책임이 없습니다.\n" +
                "고객님의 휴대폰번호는 안전하게 안심번호로\n" +
                "변환되어 등록이 되며, 다른 회원님들이 고객님께\n" +
                "연락을 할 경우 안심번호로 연결이 됩니다.";
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.policy_dialog, null);
        final TextView policy_textview =(TextView)view.findViewById(R.id.policy_textview);
        policy_textview.setText(policy_text);
        final ImageButton cancelBtn =(ImageButton)view.findViewById(R.id.cancel_btn);
        final Button policy_pre_agreeBtn =(Button)view.findViewById(R.id.policy_pre_agreement_btn);
        final Button policy_agreeBtn =(Button)view.findViewById(R.id.policy_agree_btn);
        policy_pre_agreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                policy_agreeBtn.setEnabled(true);
            }
        });

        policy_agreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    MyLogs.v(param_sub_category_item.getC_name());
                Call<Register_CallBackItem> callback = service.witness_post_register(
                        param_sub_category_item.getC_id(),
                        param_sub_category_item.getC_name(),
                        param_sub_category_item.getC_number(),
                        param_post_detail_item.getP_user_id(),
                        param_post_detail_item.getP_title(),
                        param_post_detail_item.getP_money(),
                        param_post_detail_item.getP_name(),
                        param_post_detail_item.getP_location_text(),
                        param_post_detail_item.getP_location_latitude(),
                        param_post_detail_item.getP_location_longitude(),
                        param_post_detail_item.getP_time(),
                        param_post_detail_item.getP_contents(),
                        param_post_detail_item.getP_location_detail_text(),
                        param_post_detail_item.getP_location_id());
                callback.enqueue(new Callback<Register_CallBackItem>() {
                    @Override
                    public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                        if(response.body().isResponseSuccess()){
                            MyLogs.v(response.body().toString());

                            if(param_post_detail_item.getPhoto() !=null && param_post_detail_item.getPhoto().size()>0){//파일존재
                                int post_id =  response.body().getData().getPost_id();
                                RequestBody post_id_str =
                                        RequestBody.create(MediaType.parse("multipart/form-data"),""+post_id);

                                for(int i =0; i<param_post_detail_item.getPhoto().size();i++){
                                    MyLogs.v(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
                                    File file = new File(param_post_detail_item.getPhoto().get(i).getPp_thum_name());
                                    MyLogs.v("file : "+file.getPath());
                                    RequestBody imageFile_body = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                                    RequestBody order_id_str =
                                            RequestBody.create(MediaType.parse("multipart/form-data"),""+(i+1));
                                    Call<Common_CallBackItem> fileCallBack  =  service.post_photo_register(imageFile_body,order_id_str,post_id_str);

                                    fileCallBack.enqueue(new Callback<Common_CallBackItem>() {
                                        @Override
                                        public void onResponse(Response<Common_CallBackItem> f_response, Retrofit retrofit) {
                                            if(f_response.body().isResponseSuccess()){
                                                MyLogs.v("Success");
                                                MyLogs.v(f_response.body().toString());
                                                getActivity().finish();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            MyLogs.v("picture : "+t.getMessage().toString());
                                        }
                                    });



                                }
                            }// if(horizontalImageLayoutAdapter.getItemCount()>0){//파일존재

                        }
                    }
                    @Override
                    public void onFailure(Throwable t) {
                        MyLogs.v(t.getMessage().toString());
                    }
                });











            }
        });
        final AlertDialog ab= new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.dismiss();
            }
        });
        WindowManager.LayoutParams params = ab.getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        ab.getWindow().setAttributes((WindowManager.LayoutParams) params);
        ab.show();
    }

    @Override

    public void onAttach(Activity activity) {

        super.onAttach(activity);

        ((Witness_register_Activity) activity).setOnKeyBackPressedListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((Witness_register_Activity) getActivity()).setOnKeyBackPressedListener(this);
    }

    @Override
    public void onBack() {
       getActivity().getSupportFragmentManager().popBackStack();
    }
}
