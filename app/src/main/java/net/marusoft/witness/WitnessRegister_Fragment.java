package net.marusoft.witness;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.soundcloud.android.crop.Crop;

import net.marusoft.common.BaseFragment;
import net.marusoft.common.CategoryMananger;
import net.marusoft.common.Category_Sub_Item;
import net.marusoft.common.Common_Category_Register_SpinnerAdapter;
import net.marusoft.common.Post_Detail_Item_Data;
import net.marusoft.common.Post_Detail_Item_Photo;
import net.marusoft.search_me.MapSelectActivity;
import net.marusoft.search_me.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import henen.HenenBoard.HorizontalImageLayoutAdapter;
import util.LocationData;
import util.MyLogs;
import util.RbPreferenceManager;
import util.StaticData;

/**
 * Created by Henen on 2016-02-04.
 */


public class WitnessRegister_Fragment extends BaseFragment implements Witness_register_Activity.onKeyBackPressedListener {
    public static final String TAG = "WITNESS_REGISTER_FRAGMENT";
    private int imageCount = 0;
    private RecyclerView mRecyclerView;
    HorizontalImageLayoutAdapter horizontalImageLayoutAdapter;
    Common_Category_Register_SpinnerAdapter common_category_register_spinnerAdapter;
    Button addImageBtn;
    Button location_change_btn;
    TextView top_location_textview;
    Spinner spinner;
    LocationData locationData = null;
    boolean location_checked_bool = false;


    EditText titleEditText;
    EditText location_editText;
    EditText money_editText;
    EditText contents_editText;
    TextView happenDate_textview;
    EditText name_editText;
    Date happen_date =null;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view =  inflater.from(getActivity()).inflate(R.layout.witness_register_fragment,null);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("");
        ((ActionBarActivity)getActivity()).setSupportActionBar(toolbar);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        StaticData.deleteCache(getActivity());//캐시 초기화
        spinner = (Spinner)view.findViewById(R.id.spinner);
        common_category_register_spinnerAdapter = new Common_Category_Register_SpinnerAdapter(getActivity(),
                CategoryMananger.getCategoryFromNumberId(getActivity(), CategoryMananger.TAG_WITNESS));
        spinner.setAdapter(common_category_register_spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (common_category_register_spinnerAdapter.getSelf_register_position() == position) {
                    showSpinnerCategoryInputDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        location_change_btn = (Button)view.findViewById(R.id.change_location_btn);
        location_change_btn.setOnClickListener(mOnClickListener);

        titleEditText = (EditText)view.findViewById(R.id.title_editText);
        money_editText = (EditText)view.findViewById(R.id.money_editText);
        location_editText = (EditText)view.findViewById(R.id.location_editText);
        contents_editText = (EditText)view.findViewById(R.id.contents_editText);
        name_editText = (EditText)view.findViewById(R.id.name_editText);

        top_location_textview = (TextView)view.findViewById(R.id.location_text_textview);

        happenDate_textview = (TextView)view.findViewById(R.id.happendate_textview);

        addImageBtn = (Button)view.findViewById(R.id.image_add_button);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
        horizontalImageLayoutAdapter = new HorizontalImageLayoutAdapter(getActivity(),mRecyclerView, R.layout.horizontalimageitem);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(horizontalImageLayoutAdapter);
        addImageBtn.setOnClickListener(mOnClickListener);
        happenDate_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });


        return view;
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){

                case R.id.image_add_button :
                    Crop.pickImage(getActivity(),WitnessRegister_Fragment.this);
                    break;


                case R.id.change_location_btn :
                    Intent intent = new Intent(getActivity(), MapSelectActivity.class);
                    startActivityForResult(intent, MapSelectActivity.REQUEST_LOCATION);
                    break;
            }
        }
    };

    @Override

    public void onAttach(Activity activity) {

        super.onAttach(activity);

        ((Witness_register_Activity) activity).setOnKeyBackPressedListener(this);

    }

    @Override
    public void onBack() {
        getActivity().finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == getActivity().RESULT_OK) {
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, result);
        }else if(requestCode == MapSelectActivity.REQUEST_LOCATION && resultCode == getActivity().RESULT_OK){
            locationData = (LocationData)result.getExtras().getSerializable("location_data");
            if(locationData !=null){
                location_checked_bool = true;
                top_location_textview.setText(locationData.getData().getFullName());
                MyLogs.v(locationData.toString());
            }

        }



        super.onActivityResult(requestCode, resultCode, result);
    }

    private void beginCrop(Uri source) {
        imageCount++;
        Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), imageCount+"img.png"));
        Crop.of(source, destination).asSquare().withMaxSize(1000,1000).start(getActivity(), WitnessRegister_Fragment.this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == getActivity().RESULT_OK) {
            MyLogs.v("OK");
            Toast.makeText(getActivity(), "OK", Toast.LENGTH_SHORT).show();

            File file = new File(Crop.getOutput(result).getPath());
            MyLogs.v(file.getAbsolutePath());
            MyLogs.v(file.getName());
            horizontalImageLayoutAdapter.addItem(Crop.getOutput(result));
            horizontalImageLayoutAdapter.notifyDataSetChanged();

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(),"Error",Toast.LENGTH_SHORT).show();
            //  MyLogs.v("OK");
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        ((Witness_register_Activity) getActivity()).setOnKeyBackPressedListener(this);
    }

    void showTimeDialog(){
        final Calendar calendar =  Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_DeviceDefault_Light_Dialog,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calendar.set(year,monthOfYear,dayOfMonth);
                        happen_date = calendar.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String happen_date_text = sdf.format(happen_date);
                        happenDate_textview.setText(happen_date_text);
                    }
                }, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    void showSpinnerCategoryInputDialog(){

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.category_add_alertdialog, null);
        final EditText editText =(EditText)view.findViewById(R.id.category_add_editText);
        final AlertDialog ab= new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton("아니오",null)
                .setNegativeButton("예",null)
                .create();
        ab.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {

                Button negative = ab.getButton(AlertDialog.BUTTON_NEGATIVE);
                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(editText.getText().length()==0){
                            MyLogs.v("here : " + editText.getText().length());
                            editText.setError("카테고리명을 입력해주세요");
                            // Toast.makeText(getApplicationContext(), "카테고리명을 입력해주세요", Toast.LENGTH_LONG).show();
                        }else{
                            common_category_register_spinnerAdapter.addCategory(editText.getText().toString());
                            spinner.setSelection(common_category_register_spinnerAdapter.getCount() - 1);
                            ab.dismiss();
                        }
                    }
                });

                Button positive = ab.getButton(AlertDialog.BUTTON_POSITIVE);
                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ab.dismiss();
                        spinner.setSelection(0);
                    }
                });


            }
        });

        ab.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                spinner.setSelection(0);
            }
        });
        WindowManager.LayoutParams params = ab.getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        ab.getWindow().setAttributes((WindowManager.LayoutParams) params);
        ab.show();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       // super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.board_finish_fragment_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            getActivity().finish();
            return true;
        }
        if (id == R.id.finish_btn) {
            //올리면됨
            //체크
            boolean check_bool = true;
            if(locationData  == null || location_checked_bool == false){
                check_bool = false;
                Toast.makeText(getActivity(),"현재 위치를 등록해주세요",Toast.LENGTH_LONG).show();
            }
            if(titleEditText.getText().toString().length()==0){
                check_bool = false;
                titleEditText.setError("글 제목을 입력해주세요");
            }

            if(location_editText.getText().toString().length()==0){
                check_bool = false;
                location_editText.setError( "목격 위치를 입력해주세요" );
            }

            if(name_editText.getText().toString().length()==0){
                check_bool = false;
                name_editText.setError( "이름을 입력해주세요" );
            }

            if(money_editText.getText().toString().length()==0){
                check_bool = false;
                money_editText.setError( "사례금을 입력해주세요" );
            }

            if(contents_editText.getText().toString().length()==0){
                check_bool = false;
                contents_editText.setError( "추가내용을 입력해주세요" );
            }

            if(happen_date == null){
                check_bool = false;
                happenDate_textview.setError("날짜를 선택해주세요");
            }



            if(check_bool==true){

                Post_Detail_Item_Data post_detail_item_data = new Post_Detail_Item_Data();
                post_detail_item_data.setP_is_solved("미해결");
                post_detail_item_data.setP_title(titleEditText.getText().toString());
                post_detail_item_data.setP_user_id(RbPreferenceManager.getUserId(getActivity()));
                post_detail_item_data.setP_name(name_editText.getText().toString());
                post_detail_item_data.setP_money(money_editText.getText().toString());
                post_detail_item_data.setP_contents(contents_editText.getText().toString());
                post_detail_item_data.setP_location_id(locationData.getData().getRegionId());
                post_detail_item_data.setP_location_text(locationData.getData().getFullName());
                post_detail_item_data.setP_location_detail_text(location_editText.getText().toString());
                post_detail_item_data.setP_location_latitude(locationData.getL_latitude());
                post_detail_item_data.setP_location_longitude(locationData.getL_longitude());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String happen_date_text = sdf.format(happen_date);
                post_detail_item_data.setP_time(happen_date_text);

                if (horizontalImageLayoutAdapter.getItemCount()>0){
                    ArrayList<Post_Detail_Item_Photo> photoArrayList = new ArrayList<>();
                    for(int i = 0; i<horizontalImageLayoutAdapter.getItemCount();i++){

                        Post_Detail_Item_Photo photo = new Post_Detail_Item_Photo();
                        photo.setPp_thum_name(horizontalImageLayoutAdapter.getmItems().get(i).getPath());
                        photoArrayList.add(photo);
                    }
                    post_detail_item_data.setPhoto(photoArrayList);
                }



                //android.support.v4.app.FragmentManager fm = getActivity().getSupportFragmentManager();
                /*FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                WitnessPreview_Fragment witnessPreview_fragment = new WitnessPreview_Fragment().newInstance(post_detail_item_data,(Category_Sub_Item)spinner.getSelectedItem());
                transaction.replace(R.id.contents_linear, witnessPreview_fragment, WitnessPreview_Fragment.TAG);
//                transaction.back

               // transaction.add(witnessPreview_fragment,"lostpreviewfragment");
//                transaction.add(R.id.contents_linear,witnessPreview_fragment);
                transaction.addToBackStack(null);
                transaction.commit();*/

                Intent intent = new Intent(getActivity(), WitnessPreview_Activity.class);
                intent.putExtra("ARG_POST_DETAIL_ITEM", post_detail_item_data);
                intent.putExtra("ARG_SUB_CATEGORY_ITEM", (Category_Sub_Item)spinner.getSelectedItem());
                startActivity(intent);




                //Post_Detail_Item  = new Post_Detail_Item();
                /*
                showBoardWriteDialog(titleEditText.getText().toString(),
                        lost_property_editText.getText().toString(),
                        feature_editText.getText().toString(),
                        RbPreferenceManager.getUserId(getActivity()),
                        location_editText.getText().toString(),
                        money_editText.getText().toString(),
                        contents_editText.getText().toString(),
                        (Category_Sub_Item)spinner.getSelectedItem(),
                        locationData);
                        */
            }







            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
