package net.marusoft.Tip;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import net.marusoft.common.BaseFragment;
import net.marusoft.common.CommonSearchMe_List_Adapter;
import net.marusoft.common.Common_List_Adapter;
import net.marusoft.common.Common_List_Data;
import net.marusoft.common.Common_SearchMeItem_CallBack;
import net.marusoft.common.EndlessRecyclerOnScrollListener;
import net.marusoft.common.Post_Detail_Item_Data;
import net.marusoft.search_me.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.DpManager;
import util.MyLogs;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-01-28. 분실물 프래그먼트
 */
public class Tip_Fragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String ARG_C_NUMBER = "MAJOR_ID";
    private static final String ARG_C_ID = "C_ID";
    private int PARAM_C_NUMBER;
    private int PARAM_C_ID;
    private int index = 0;
    private int count = 20;
    boolean allItemLoaded = false;
    CommonSearchMe_List_Adapter commonSearchMeListAdapter;
    private SwipeRefreshLayout mSwipeRefresh;

    View v;
    public Tip_Fragment newInstance(int c_number,int c_id) {
        Tip_Fragment tip_Fragment = new Tip_Fragment();
        Bundle args = new Bundle();
        args.putInt(ARG_C_NUMBER, c_number);
        args.putInt(ARG_C_ID, c_id);
        tip_Fragment.setArguments(args);
        return tip_Fragment;
    }

    @Bind(R.id.cl_recycleView)
    RecyclerView cl_recycleView;
    Common_List_Adapter adapter;
    ArrayList<Common_List_Data> data_list = new ArrayList<>();

    @Override
    public void onResume() {
        super.onResume();
        if(StaticData.refresh_bool) {
            StaticData.refresh_bool = false;
            allItemLoaded = false;
            index = 0;
            get_data();
        } else {
            StaticData.refresh_bool = false;
        }
    }

    @Override
    public void onRefresh() {
        allItemLoaded = false;
        index = 0;
        get_data();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            PARAM_C_NUMBER  = getArguments().getInt(ARG_C_NUMBER);
            PARAM_C_ID  = getArguments().getInt(ARG_C_ID);
            setHasOptionsMenu(true);
        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //setCustomActionbar();


        v = inflater.inflate(R.layout.common_list_fragment, container, false);
        ButterKnife.bind(this, v);

        cl_recycleView.setHasFixedSize(true);
        cl_recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mSwipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swype);
        mSwipeRefresh.setOnRefreshListener(this);

        get_data();

        return v;
    }


    void get_data() {
        mSwipeRefresh.setRefreshing(true);
        Call<Common_SearchMeItem_CallBack> call = service.post_list(PARAM_C_ID,index,count);
        call.enqueue(new Callback<Common_SearchMeItem_CallBack>() {
            @Override
            public void onResponse(Response<Common_SearchMeItem_CallBack> response, Retrofit retrofit) {
                ArrayList<Post_Detail_Item_Data> dataArrayList = (ArrayList<Post_Detail_Item_Data>)response.body().getData();
                commonSearchMeListAdapter = new CommonSearchMe_List_Adapter(getActivity(),dataArrayList);
                cl_recycleView.setAdapter(commonSearchMeListAdapter);
                index = dataArrayList.size();
                if(index == 0) {
                    v.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
                } else {
                    v.findViewById(R.id.empty_view).setVisibility(View.GONE);
                }
                mSwipeRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        cl_recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                Call<Common_SearchMeItem_CallBack> call = service.post_list(PARAM_C_ID, index, count);
                if (allItemLoaded == false) {

                    call.enqueue(new Callback<Common_SearchMeItem_CallBack>() {
                        @Override
                        public void onResponse(Response<Common_SearchMeItem_CallBack> response, Retrofit retrofit) {
                            MyLogs.v("index : " + index + " count : " + count);
                            ArrayList<Post_Detail_Item_Data> dataArrayList = (ArrayList<Post_Detail_Item_Data>) response.body().getData();


                            if (dataArrayList.size() == 0) {
                                allItemLoaded = true;
                                // Toast.makeText(getActivity(),"마지막 글입니다",Toast.LENGTH_LONG).show();
                            } else {
                                commonSearchMeListAdapter.addItems(dataArrayList);
                                index = commonSearchMeListAdapter.getItemCount() - 1;
                            }
                            mSwipeRefresh.setRefreshing(false);
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                }
            }
        });
    }

    private void setCustomActionbar(){

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        ImageButton home_btn;
        ImageButton write_btn;
        View mCustomView = LayoutInflater.from(getActivity()).inflate(R.layout.register_actionbar_main,null);
        home_btn = (ImageButton)mCustomView.findViewById(R.id.left_imgBtn);

        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        write_btn = (ImageButton)mCustomView.findViewById(R.id.right_imgBtn);
        write_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Tip_register_Activity.class);
                startActivity(intent);
            }
        });


        actionBar.setCustomView(mCustomView);
        Toolbar parent = (Toolbar)mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        parent.setPadding(0,0, (int)DpManager.convertDpToPixel(20,getActivity()),0);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F88F24")));
    }




}
