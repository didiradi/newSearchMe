package net.marusoft.search_me;

import net.marusoft.common.Post_Detail_Item_Data;

import java.util.List;

/**
 * Created by SEO on 2016-02-12.
 */
public class FavoriteListCallBack {

    private List<Data> data;

    private Meta meta;

    public List<Data>  getData ()
    {
        return data;
    }

    public void setData (List<Data> datas)
    {
        this.data = data;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }

    public class Meta
    {
        private String state;

        private String code;

        private String msg;

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getMsg ()
        {
            return msg;
        }

        public void setMsg (String msg)
        {
            this.msg = msg;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", code = "+code+", msg = "+msg+"]";
        }
    }

    public class Data
    {
        private Post_Detail_Item_Data post;

        private String f_id;

        private String f_write_date;

        private String f_post_id;

        private String f_user_id;

        public Post_Detail_Item_Data getPost ()
        {
            return post;
        }

        public void setPost (Post_Detail_Item_Data post)
        {
            this.post = post;
        }

        public String getF_id ()
        {
            return f_id;
        }

        public void setF_id (String f_id)
        {
            this.f_id = f_id;
        }

        public String getF_write_date ()
        {
            return f_write_date;
        }

        public void setF_write_date (String f_write_date)
        {
            this.f_write_date = f_write_date;
        }

        public String getF_post_id ()
        {
            return f_post_id;
        }

        public void setF_post_id (String f_post_id)
        {
            this.f_post_id = f_post_id;
        }

        public String getF_user_id ()
        {
            return f_user_id;
        }

        public void setF_user_id (String f_user_id)
        {
            this.f_user_id = f_user_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [post = "+post+", f_id = "+f_id+", f_write_date = "+f_write_date+", f_post_id = "+f_post_id+", f_user_id = "+f_user_id+"]";
        }
    }
}
