package net.marusoft.search_me;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;

import util.MyLogs;
import util.RbPreference;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver
{
	private RbPreference pref;
	@Override
	public void onReceive(Context context, Intent intent)
	{
		pref = new RbPreference(context);
		if(pref.getValue(RbPreference.PUSH_STATE, true) == true) {
			Bundle bundle = intent.getExtras();
			for (String key : bundle.keySet())
			{
				Object value = bundle.get(key);
				MyLogs.v(String.format("%s : %s (%s)", key, value.toString(), value.getClass().getName()) + "|");
			}
			MyLogs.v("|" + "=================" + "|");
	
			ComponentName comp = new ComponentName(context.getPackageName(), GCMIntentService.class.getName());
	
			startWakefulService(context, intent.setComponent(comp));
			setResultCode(Activity.RESULT_OK);
		} else {
			//푸시OFF상태
		}
	}
}
