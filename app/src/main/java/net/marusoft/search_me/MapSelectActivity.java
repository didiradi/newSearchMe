package net.marusoft.search_me;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;
import net.marusoft.common.BaseActionbarActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import util.LocationData;
import util.MyLogs;
import util.RbPreference;
import util.StaticData;

/**
 * Created by MARUSOFT-CHOI on 2016-02-01.
 */
public class MapSelectActivity extends BaseActionbarActivity implements MapView.MapViewEventListener {
    public static final int REQUEST_LOCATION = 1337;
    @Bind(R.id.ms_recycleView)
    RecyclerView ms_recycleView;
    @Bind(R.id.ms_editText)
    EditText ms_editText;
    @Bind(R.id.empty_view)
    TextView empty_view;
    private Context context;
    private Address_Search_Adapter adapter;
    private Address_Point_Data data;
    private String finalLongitude = String.valueOf(127.00557633);
    private String finalLatitude = String.valueOf(37.53737528);
    private Address_Data address_data;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapselect_activity);
        ButterKnife.bind(this);
        context = this;

        empty_view = (TextView) findViewById(R.id.empty_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final MapView mapView = new MapView(context);
        mapView.setDaumMapApiKey(StaticData.DAUMMAPKEY);

        ViewGroup mapViewContainer = (ViewGroup) findViewById(R.id.map_view);
        mapViewContainer.addView(mapView);

        // 중심점 변경
        if(StaticData.initPreference(context).getValue(RbPreference.LASTLATITUDE, 0.0f) == 0.0 && StaticData.initPreference(context).getValue(RbPreference.LASTLONGITUDE, 0.0f) == 0.0) {
            mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(37.53737528, 127.00557633), true);
        } else {
            mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(StaticData.initPreference(context).getValue(RbPreference.LASTLATITUDE, 0.0f), StaticData.initPreference(context).getValue(RbPreference.LASTLONGITUDE, 0.0f)), true);
        }

        //줌레벨
        mapView.setZoomLevel(3, true);

        mapView.setMapViewEventListener(this); // this에 MapView.MapViewEventListener 구현.

        ms_recycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        MyLogs.v("getposition" + data.getChannel().getItem().get(position).getPoint_x());
                        finalLongitude = data.getChannel().getItem().get(position).getPoint_x();
                        finalLatitude = data.getChannel().getItem().get(position).getPoint_y();
                        // 중심점 변경
                        mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(Double.parseDouble(finalLatitude), Double.parseDouble(finalLongitude)), true);

                    }
                })
        );

        ms_editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_SEARCH) {
                    getAddressJson(ms_editText.getText().toString());
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });

        empty_view.setVisibility(View.VISIBLE);
    }

    public void hideKeyboard(){ //키보드숨기기

        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    @OnClick(R.id.ms_searchBtn)
    void msSearchBtn() {
        getAddressJson(ms_editText.getText().toString());
        hideKeyboard();
    }

    Address_Point_Data getAddressJson(String address_value) {
        data = new Address_Point_Data();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StaticData.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Address_Point_Data> call = service.address_point_data(StaticData.DAUMMAPKEY, address_value, 30, "json");
        call.enqueue(new Callback<Address_Point_Data>() {
            @Override
            public void onResponse(Response<Address_Point_Data> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    data = response.body();
                    adapter = new Address_Search_Adapter(context, data, R.layout.mapselect_activity);
                    ms_recycleView.setHasFixedSize(true);
                    ms_recycleView.setLayoutManager(new LinearLayoutManager(context));
                    ms_recycleView.setAdapter(adapter);
                    MyLogs.v("retrofit point data : " + data.getChannel().getItem().toString());

                    if(data.getChannel().getTotalCount() == 0) {
                        empty_view.setVisibility(View.VISIBLE);
                    } else {
                        empty_view.setVisibility(View.GONE);
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Throwable t) {
                MyLogs.v("Fail");
            }
        });
        return data;
    }

    @Override
    public void onMapViewInitialized(MapView mapView) {

    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {
        MyLogs.v("movecenter : " + mapPoint);
    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
        MyLogs.v("finish : " + mapPoint.getMapPointGeoCoord().latitude);
        MyLogs.v("finish : " + mapPoint.getMapPointGeoCoord().longitude);
        finalLongitude = String.valueOf(mapPoint.getMapPointGeoCoord().longitude);
        finalLatitude = String.valueOf(mapPoint.getMapPointGeoCoord().latitude);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mapselect_activity_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.finish_btn :
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(StaticData.API_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                APIService service = retrofit.create(APIService.class);
                Call<Address_Data> call = service.addressdata(StaticData.DAUMMAPKEY, Double.parseDouble(finalLongitude), Double.parseDouble(finalLatitude), "WGS84", "json");
                call.enqueue(new Callback<Address_Data>() {
                    @Override
                    public void onResponse(Response<Address_Data> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            address_data = response.body();
                            MyLogs.v("retrofit data : " + address_data.toString());
                            MyLogs.v("address_fi" + address_data.getFullName());
                            Intent intent = new Intent();
                            LocationData data = new LocationData(address_data, finalLongitude, finalLatitude);
                            intent.putExtra("location_data", data);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        MyLogs.v("Fail");
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }
}
