package net.marusoft.search_me;

import java.util.List;

/**
 * Created by MARUSOFT-CHOI on 2016-01-29.
 */
public class Address_Point_Data {
    public Channel channel;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public class Channel {
        int result;
        int totalCount;
        public List<Item> item;

        public int getResult() {
            return result;
        }

        public void setResult(int result) {
            this.result = result;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return "Channel{" +
                    "result=" + result +
                    ", totalCount=" + totalCount +
                    ", item=" + item +
                    '}';
        }

        public class Item {
            String id;
            String title;
            String point_x;
            String point_y;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPoint_x() {
                return point_x;
            }

            public void setPoint_x(String point_x) {
                this.point_x = point_x;
            }

            public String getPoint_y() {
                return point_y;
            }

            public void setPoint_y(String point_y) {
                this.point_y = point_y;
            }

            @Override
            public String toString() {
                return "Item{" +
                        "id='" + id + '\'' +
                        ", title='" + title + '\'' +
                        ", point_x='" + point_x + '\'' +
                        ", point_y='" + point_y + '\'' +
                        '}';
            }
        }
    }

    @Override
    public String toString() {
        return "Address_Point_Data{" +
                "channel=" + channel +
                '}';
    }
}
