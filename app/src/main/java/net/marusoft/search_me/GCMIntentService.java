package net.marusoft.search_me;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import util.MyLogs;

public class GCMIntentService extends IntentService
{
  public static final int NOTIFICATION_ID = 1;
  
  
  public GCMIntentService()
  {
    super("GCMIntentService");
  }
  
  
  @Override
  protected void onHandleIntent(Intent intent)
  {
    Bundle extras = intent.getExtras();
    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
    // The getMessageType() intent parameter must be the intent you received
    // in your BroadcastReceiver.
    String messageType = gcm.getMessageType(intent);
    
    if (!extras.isEmpty())
    { // has effect of unparcelling Bundle
      /*
       * Filter messages based on message type. Since it is likely that GCM will
       * be extended in the future with new message types, just ignore any
       * message types you're not interested in, or that you don't recognize.
       */
      if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType))
      {
        sendNotification(extras.toString(), extras.toString(), extras.toString(), extras.toString());
      }
      else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType))
      {
        // If it's a regular GCM message, do some work.
      }
      else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
      {
    	String title = intent.getStringExtra("title");
        String msg = intent.getStringExtra("msg");
        String post_id = intent.getStringExtra("post_id");
        String category_id = intent.getStringExtra("category_id");
        // Post notification of received message.
//            sendNotification("Received: " + extras.toString());
        sendNotification(title, msg, post_id, category_id);
        MyLogs.v("Received: " + extras.toString());
      }
    }
    // Release the wake lock provided by the WakefulBroadcastReceiver.
    GcmBroadcastReceiver.completeWakefulIntent(intent);
  }
  
  
  // Put the message into a notification and post it.
  // This is just one simple example of what you might choose to do with
  // a GCM message.
  private void sendNotification(String title, String msg, String post_id, String category_id)
  {
    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    
    Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    
    Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.putExtra("title", title);
    intent.putExtra("msg", msg);
    intent.putExtra("post_id", post_id);
    intent.putExtra("category_id", category_id);

    MyLogs.v("push_data : " + title + ", " + msg + ", " + post_id + ", " + category_id);
    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    
    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher).setContentTitle(title).setStyle(
        new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg).setAutoCancel(true).setVibrate(new long[] { 0, 500 });
    mBuilder.setContentIntent(contentIntent);
    mBuilder.setSound(uri);
    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
  }
}
