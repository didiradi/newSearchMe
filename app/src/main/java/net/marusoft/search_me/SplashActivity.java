package net.marusoft.search_me;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import net.htmlparser.jericho.Source;
import net.marusoft.common.CategoryMananger;
import net.marusoft.detail_activity.Activity_LostPerson_Detail;
import net.marusoft.detail_activity.Activity_Person_Detail;
import net.marusoft.detail_activity.Activity_Pet_Detail;
import net.marusoft.detail_activity.Activity_Tip_Detail;
import net.marusoft.detail_activity.Activity_Wanted_Detail;
import net.marusoft.detail_activity.Activity_Witness_Detail;
import net.marusoft.detail_activity.LostAndFound_Detail_Activity;

import java.io.IOException;

import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;
import main_register.Phone_Register_Activity;
import util.MyLogs;
import util.NetworkCheck;
import util.RbPreference;
import util.StaticData;
/**
 * Created by MARUSOFT-CHOI on 2016-02-03.
 */
public class SplashActivity extends Activity {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SENDER_ID = "539042598541";
    private Context context;
    private GoogleCloudMessaging _gcm;
    private String _regId;
    private String post_id;
    private String category_id;
    public static float server_ServerVersion;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        context = this;
        //MyLogs.LOGV = false; //모든 로그 안보이게 함
        permissionAllow();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }


    public void permissionAllow(){
        PermissionGen.with(SplashActivity.this)
                .addRequestCode(100)
                .permissions(
                        android.Manifest.permission.INTERNET,
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        android.Manifest.permission.ACCESS_NETWORK_STATE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.RECEIVE_SMS
                ).request();
    }

    @PermissionSuccess(requestCode = 100)
    public void doSomething(){

        StaticData.initPreference(context).put(RbPreference.DEVICEID, getDeviceId());
        Intent intent = this.getIntent();
        if(intent != null) {
            post_id = intent.getStringExtra("post_id");
            category_id = intent.getStringExtra("category_id");
            MyLogs.v("splash post_id : " + post_id);
        } else {
            MyLogs.v("splash post_id : " + "null");
        }

        if(NetworkCheck.isOnline(context)) {
            new Version_Thread(context, StaticData.SEARCHME_URL + "android_version.html").execute();
        } else {
            Toast.makeText(context, "네트워크 연결을 확인해주세요.", Toast.LENGTH_SHORT).show();
            finish();
        }



    }

    // 장치 아이디 얻기
    public String getDeviceId()
    {
        TelephonyManager mgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return mgr.getDeviceId();
    }

    // google play service가 사용가능한가
    private boolean checkPlayServices()
    {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else
            {
                MyLogs.v("|This device is not supported.|");
                //_textStatus.append("\n This device is not supported.\n");
                finish();
            }
            return false;
        }
        return true;
    }


    // registration  id를 가져온다.
    private String getRegistrationId()
    {
        String registrationId = PreferenceUtil.instance(getApplicationContext()).regId();
        if (TextUtils.isEmpty(registrationId))
        {
            MyLogs.v("|Registration not found.|");
            //_textStatus.append("\n Registration not found.\n");
            return "";
        }
        int registeredVersion = PreferenceUtil.instance(getApplicationContext()).appVersion();
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion)
        {
            MyLogs.v("|App version changed.|");
            //_textStatus.append("\n App version changed.\n");
            return "";
        }
        return registrationId;
    }

    // app version을 가져온다. 뭐에 쓰는건지는 모르겠다.
    private int getAppVersion()
    {
        try
        {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    // gcm 서버에 접속해서 registration id를 발급받는다.
    private void registerInBackground()
    {
        new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... params)
            {
                String msg = "";
                try
                {
                    if (_gcm == null)
                    {
                        _gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    _regId = _gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + _regId;

                    StaticData.initPreference(context).put(RbPreference.REGID, _regId);

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(_regId);
                }
                catch (IOException ex)
                {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }

                return msg;
            }


            @Override
            protected void onPostExecute(String msg)
            {
                MyLogs.v("regid1 : " +msg);
                //_textStatus.append(msg);
            }
        }.execute(null, null, null);
    }


    // registraion id를 preference에 저장한다.
    private void storeRegistrationId(String regId)
    {
        int appVersion = getAppVersion();
        MyLogs.v("Saving regId on app version " + appVersion + "|");
        PreferenceUtil.instance(getApplicationContext()).putRedId(regId);
        PreferenceUtil.instance(getApplicationContext()).putAppVersion(appVersion);


    }
    @PermissionFail(requestCode = 100)
    public void doFailSomething(){
        AlertDialog.Builder ab = new AlertDialog.Builder(SplashActivity.this,android.R.style.Theme_Material_Light_Dialog)
                .setTitle("써치미")
                .setMessage("권한을 설정하지 않으시면 앱을 이용하실수 없습니다.")
                .setPositiveButton("앱 종료", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("설정하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionAllow();
                    }
                }).setCancelable(false);

        ab.show();
    }

    public class Version_Thread extends AsyncTask<String, String, String> {
        private Context v_context;
        Source ser_source = null;
        String sermAddr;
        String s_version;
        public Version_Thread(Context context, String seraddr) {
            super();
            this.v_context = context;
            sermAddr = seraddr;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                ser_source = new Source(new java.net.URL(sermAddr));
            } catch (IOException e) {
                e.printStackTrace();
            }

            ser_source.fullSequentialParse();
            s_version = ser_source.getTextExtractor().toString();
            MyLogs.v("s_version : " + s_version);
            return s_version;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            server_ServerVersion = Float.parseFloat(result.replace("version:", ""));
            float current_serverVersion = Float.parseFloat(getResources().getString(R.string.check_version).replace("version:", ""));
            MyLogs.v("s_versio1 : " + server_ServerVersion + " : " + current_serverVersion);

            if(server_ServerVersion > current_serverVersion) { //업데이트
                DialogUpdate();
            } else if(server_ServerVersion < current_serverVersion) {//점검중
                DialogCheck();
            } else if(server_ServerVersion == current_serverVersion) { //버전같음
                // google play service가 사용가능한가
                if (checkPlayServices())
                {
                    _gcm = GoogleCloudMessaging.getInstance(context);
                    _regId = getRegistrationId();

                    if (TextUtils.isEmpty(_regId)) { //regid가 비어있으면
                        registerInBackground();
                    } else {
                        StaticData.initPreference(context).put(RbPreference.REGID, _regId);
                        MyLogs.v("regid2 : " + getRegistrationId());
                    }

                }
                else
                {
                    MyLogs.v("|No valid Google Play Services APK found.|");
                    //_textStatus.append("\n No valid Google Play Services APK found.\n");
                }


                Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        CategoryMananger.DownLoadDB(getApplicationContext());
                        if(StaticData.initPreference(SplashActivity.this).getValue(RbPreference.LOGIN_STATE, false)) {
                            if(post_id != null && category_id != null) {
                                int c_category_id = Integer.parseInt(category_id);

                                Intent intent1 = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(intent1);
                                finish();

                                if((c_category_id >= 8 && c_category_id <= 13) || c_category_id == 36) { //현상범
                                    Intent intent = new Intent(SplashActivity.this, Activity_Wanted_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id >= 14 && c_category_id <= 17) { //분실물
                                    Intent intent = new Intent(SplashActivity.this, LostAndFound_Detail_Activity.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id >= 18 && c_category_id <= 21) { //실종
                                    Intent intent = new Intent(SplashActivity.this, Activity_LostPerson_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id >= 22 && c_category_id <= 27) { //제보자를 찾습니다
                                    Intent intent = new Intent(SplashActivity.this, Activity_Tip_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id >= 28 && c_category_id <= 30) { //목격자를 찾습니다
                                    Intent intent = new Intent(SplashActivity.this, Activity_Witness_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id >= 31 && c_category_id <= 34) { //사람을 찾습니다
                                    Intent intent = new Intent(SplashActivity.this, Activity_Person_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                } else if(c_category_id == 35) { //반려동물
                                    Intent intent = new Intent(SplashActivity.this, Activity_Pet_Detail.class);
                                    intent.putExtra("p_id", post_id);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }

                        } else {
                            //StaticData.initPreference(context).put(RbPreference.USER_PHONENUM, "010-2794-5939");
                            //Intent intent = new Intent(SplashActivity.this, Auth_Register_Activity.class);
                            Intent intent = new Intent(SplashActivity.this, Phone_Register_Activity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                };

                handler.sendEmptyMessageDelayed(0, 1000);
            }

        }
    }

    private void DialogUpdate() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage("써치미를 사용하기 위해서는 업데이트를 해야합니다.").setCancelable(false);
        dlg.setPositiveButton("업데이트",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=net.marusoft.search_me"));

                        startActivity(intent);
                        finish();
                    }
                });
        dlg.setNegativeButton("취소",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        finish();
                    }
                });
        AlertDialog alert = dlg.create();
        alert.setTitle("업데이트");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.show();
    }

    private void DialogCheck() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage("서버에서 점검중입니다. 잠시 후에 이용해주세요.").setCancelable(false);
        dlg.setPositiveButton("확인",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        finish();
                    }
                });

        AlertDialog alert = dlg.create();
        alert.setTitle("점검중..");
        alert.setIcon(R.mipmap.ic_launcher);
        alert.show();
    }
}

