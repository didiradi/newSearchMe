package net.marusoft.search_me;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import net.marusoft.Tip.Tip_MainActivity;
import net.marusoft.common.BaseActivity;
import net.marusoft.common.Register_CallBackItem;
import net.marusoft.lost_and_found.LostAndFound_MainActivity;
import net.marusoft.lost_person.LostPerson_MainActivity;
import net.marusoft.person.Person_MainActivity;
import net.marusoft.pet.Pet_MainActivity;
import net.marusoft.wanted.Wanted_MainActivity;
import net.marusoft.witness.Witness_MainActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my_setting.My_Setting_Activity;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.BackPressCloseHandler;
import util.GpsInfo;
import util.LocationData;
import util.MyLogs;
import util.MyProgressDialog;
import util.RbPreference;
import util.StaticData;

public class MainActivity extends BaseActivity {
    /*@Bind(R.id.main_ibtn1) ImageButton main_ibtn1; // 현상범
    @Bind(R.id.main_ibtn2) ImageButton main_ibtn2; // 목격자를 찾습니다.
    @Bind(R.id.main_ibtn3) ImageButton main_ibtn3; // 사람을 찾습니다.
    @Bind(R.id.main_ibtn4) ImageButton main_ibtn4; // 제보를 받습니다.
    @Bind(R.id.main_ibtn5) ImageButton main_ibtn5; // 분실물
    @Bind(R.id.main_ibtn6) ImageButton main_ibtn6; // 실종*/
    @Bind(R.id.main_address_tv) TextView main_address_tv;

    public static final int SEARCH_TIME = 10;
    public GpsInfo gps;
    private TimerTask mTask;
    private Timer mTimer;
    private int timer_value = 0;
    public double latitude = 0.0;
    public double longitude = 0.0;
    public Context context;
    private Address_Data address_data;
    private RbPreference pre;

    private BackPressCloseHandler backPressCloseHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      //  setCustomActionbar();
        backPressCloseHandler = new BackPressCloseHandler(this);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_star_drawable, null));
        else
            getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_star_drawable));



        context = MainActivity.this;

        pre = new RbPreference(context);
        ButterKnife.bind(this);

        chkGpsService();
    }

    @OnClick(R.id.main_ibtn1)
    void ibtn1() {
        Intent intent = new Intent(this, Wanted_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.main_ibtn2)
    void ibtn2() {
        Intent intent = new Intent(this, Pet_MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_ibtn3)
    void ibtn3() {
        Intent intent = new Intent(this, LostAndFound_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.main_ibtn4)
    void ibtn4()
    {
        Intent intent = new Intent(this, LostPerson_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.main_ibtn5)
    void ibtn5() {
        Intent intent = new Intent(this, Tip_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.main_ibtn6)
    void ibtn6() {
        Intent intent = new Intent(this, Witness_MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.main_ibtn7)
    void ibtn7() {
        Intent intent = new Intent(this, Person_MainActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.address_search_ibtn)
    void search_ibtn(){
        Intent intent = new Intent(context, MapSelectActivity.class);
        startActivityForResult(intent, MapSelectActivity.REQUEST_LOCATION);
    }


    private void setCustomActionbar(){

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        ImageButton favoriteBtn;
        ImageButton myPageBtn;
        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_main,null);
        favoriteBtn = (ImageButton)mCustomView.findViewById(R.id.left_imgBtn);
        myPageBtn = (ImageButton)mCustomView.findViewById(R.id.right_imgBtn);

        myPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.initPreference(context).put(RbPreference.ACTIVITYCHOICE, 1);
                Intent intent = new Intent(context, My_Setting_Activity.class);
                startActivity(intent);
            }
        });


        favoriteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FavoriteItem_Activity.class);
                startActivity(intent);
            }
        });





        actionBar.setCustomView(mCustomView);
        Toolbar parent = (Toolbar)mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F88F24")));
    }

    //GPS 설정 체크
    private boolean chkGpsService() {
        gps = new GpsInfo(context);
        String c_gps = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!(c_gps.matches(".*gps.*") && c_gps.matches(".*network.*"))) {

            // GPS OFF 일때 Dialog 표시
            AlertDialog.Builder gsDialog = new AlertDialog.Builder(this);
            gsDialog.setTitle("위치 서비스 설정");
            gsDialog.setCancelable(false);
            gsDialog.setMessage("무선 네트워크 사용, GPS 위성 사용을 모두 체크하셔야 정확한 위치 서비스가 가능합니다.\n위치 서비스 기능을 설정하시겠습니까?");
            gsDialog.setPositiveButton("환경설정", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // GPS설정 화면으로 이동
                    Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    startActivityForResult(intent, 0);
                }
            })
                    .setNegativeButton("위치검색", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(context, MapSelectActivity.class);
                            startActivityForResult(intent, MapSelectActivity.REQUEST_LOCATION);
                        }
                    }).create().show();
            return false;

        } else {
            gps = new GpsInfo(context);
            StaticData.get_gpsjson(context, main_address_tv, gps.getLongitude(), gps.getLatitude());
            //Toast.makeText(context, ""+gps.getLatitude() + gps.getLongitude(), Toast.LENGTH_SHORT).show();
            pre.put(pre.LASTLATITUDE, (float)gps.getLatitude());
            pre.put(pre.LASTLONGITUDE, (float)gps.getLongitude());
           /* StaticData.finalLongitude = gps.getLongitude();
            StaticData.finalLatitude = gps.getLatitude();*/
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if(resultCode == 0 || resultCode == -1) { //위치정보창에서 받는 리턴값
                    MyLogs.v("들어옴");
                    // GPS 사용유무 가져오기
                    MyProgressDialog.showDialog(context, "현재 위치를 찾는중입니다.");
                    mTask = new TimerTask() {
                        @Override
                        public void run() {
                            if(timer_value < SEARCH_TIME) { //10초동안 탐색
                                if(latitude == 0.0) { //gps안잡힐때
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run() {
                                                    gps = new GpsInfo(context);
                                                    latitude = gps.getLatitude();
                                                    longitude = gps.getLongitude();
                                                    timer_value++;
                                                }
                                            });
                                        }
                                    }).start();
                                } else {
                                    mTimer.cancel();
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            runOnUiThread(new Runnable(){
                                                @Override
                                                public void run() {
                                                    MyProgressDialog.disMissDialog();
                                                    mTimer.cancel();
                                                    Toast.makeText(context, "위치가 잡혔습니다.!", Toast.LENGTH_SHORT).show();
                                                    StaticData.get_gpsjson(context, main_address_tv, gps.getLongitude(), gps.getLatitude());
                                                    pre.put(pre.LASTLATITUDE, (float)gps.getLatitude());
                                                    pre.put(pre.LASTLONGITUDE, (float)gps.getLongitude());
                                                    /*StaticData.finalLongitude = gps.getLongitude();
                                                    StaticData.finalLatitude = gps.getLatitude();*/
                                                }
                                            });
                                        }
                                    }).start();
                                }
                            } else {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        runOnUiThread(new Runnable(){
                                            @Override
                                            public void run() {
                                                MyProgressDialog.disMissDialog();
                                                mTimer.cancel();
                                                Toast.makeText(context, "시간이 초과되어 앱을 다시 실행 하여주세요!", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        });
                                    }
                                }).start();

                                //finish();
                            }

                        }
                    };

                    mTimer = new Timer();

                    mTimer.schedule(mTask, 1000, 1000);
                }

                break;
            case MapSelectActivity.REQUEST_LOCATION:
                if(resultCode == RESULT_OK) {
                    /*String address_title = data.getStringExtra("address_title");
                    MyLogs.v("address_title : " + address_title);
                    main_address_tv.setText(address_title);*/
                    String longitude = null, latitude = null;
                    longitude = data.getStringExtra("longitude");
                    latitude = data.getStringExtra("latitude");

                    LocationData location_data =(LocationData)data.getSerializableExtra("location_data");
                    MyLogs.v("l_address : " + location_data.getData().getFullName());
                    MyLogs.v("longitude : " + location_data.getL_longitude());
                    MyLogs.v("latitude : " + location_data.getL_latitude());

                    pre.put(pre.CURRENT_ADDRESS, location_data.getData().getFullName());
                    pre.put(pre.LASTLATITUDE, Float.parseFloat(location_data.getL_latitude()));
                    pre.put(pre.LASTLONGITUDE, Float.parseFloat(location_data.getL_longitude()));

                    main_address_tv.setText(location_data.getData().getFullName());
                    //MyLogs.v("address : " + StaticData.get_gpsjson(Double.parseDouble(longitude), Double.parseDouble(latitude)));
                    //StaticData.get_gpsjson(Double.parseDouble(location_data.getL_longitude()), Double.parseDouble(location_data.getL_latitude()));
                    putFixlocation(location_data);
                }
                break;
            default:


                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if(id ==android.R.id.home){
            Intent intent = new Intent(context, FavoriteItem_Activity.class);
            startActivity(intent);
            return true;
        }else
        if (id == R.id.setting_btn) {
            StaticData.initPreference(context).put(RbPreference.ACTIVITYCHOICE, 1);
            Intent intent = new Intent(context, My_Setting_Activity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        MyLogs.v("timer destroy");
        if(mTimer!=null) {
            mTimer.cancel();
        }
        super.onDestroy();
    }

    void putFixlocation(LocationData location_data) {
        MyProgressDialog.showDialog(context, "위치 변경하는중...");
        Call<Register_CallBackItem> callback = service.user_location_register(pre.getValue(RbPreference.USER_NUM, ""), location_data.getData().getRegionId(), location_data.getData().getFullName(), "n", location_data.getL_latitude(), location_data.getL_longitude());
        callback.enqueue(new Callback<Register_CallBackItem>() {
            @Override
            public void onResponse(Response<Register_CallBackItem> response, Retrofit retrofit) {
                if (response.body().isResponseSuccess()) {
                    Toast.makeText(context, "위치 변경이 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                } else {
                    Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
                    MyProgressDialog.disMissDialog();
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        backPressCloseHandler.onBackPressed();
    }
}
