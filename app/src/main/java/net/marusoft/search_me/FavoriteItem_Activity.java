package net.marusoft.search_me;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import net.marusoft.common.BaseActivity;
import net.marusoft.common.CommonSearchMe_List_Adapter;
import net.marusoft.common.Common_List_Adapter;
import net.marusoft.common.Common_List_Data;
import net.marusoft.common.EndlessRecyclerOnScrollListener;
import net.marusoft.common.Post_Detail_Item_Data;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import util.MyLogs;
import util.RbPreferenceManager;

public class FavoriteItem_Activity extends BaseActivity {

    private int index = 0;
    private int count = 20;
    boolean allItemLoaded = false;
    CommonSearchMe_List_Adapter commonSearchMeListAdapter;

    @Bind(R.id.cl_recycleView)
    RecyclerView cl_recycleView;
    Common_List_Adapter adapter;
    ArrayList<Common_List_Data> data_list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_item_activity);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cl_recycleView.setHasFixedSize(true);
        cl_recycleView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        Call<FavoriteListCallBack> call = service.favorite_list(RbPreferenceManager.getUserId(FavoriteItem_Activity.this),index,count);
        MyLogs.v(RbPreferenceManager.getUserId(FavoriteItem_Activity.this));

        call.enqueue(new Callback<FavoriteListCallBack>() {
            @Override
            public void onResponse(Response<FavoriteListCallBack> response, Retrofit retrofit) {
                MyLogs.v(response.body().toString());
                ArrayList<FavoriteListCallBack.Data> dataArrayList2 = (ArrayList<FavoriteListCallBack.Data>) response.body().getData();
                ArrayList<Post_Detail_Item_Data> dataArrayList = new ArrayList<>();

                for (FavoriteListCallBack.Data items :dataArrayList2){
                    dataArrayList.add(items.getPost());
                }

                commonSearchMeListAdapter = new CommonSearchMe_List_Adapter(getApplicationContext(), dataArrayList);
                cl_recycleView.setAdapter(commonSearchMeListAdapter);
                index = dataArrayList.size();
                if(index == 0) {
                    findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.empty_view).setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        cl_recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int currentPage) {
                Call<FavoriteListCallBack> call = service.favorite_list(RbPreferenceManager.getUserId(FavoriteItem_Activity.this), index, count);
                if (allItemLoaded == false) {

                    call.enqueue(new Callback<FavoriteListCallBack>() {
                        @Override
                        public void onResponse(Response<FavoriteListCallBack> response, Retrofit retrofit) {

                            MyLogs.v("index : " + index + " count : " + count);
                            ArrayList<FavoriteListCallBack.Data> dataArrayList2 = (ArrayList<FavoriteListCallBack.Data>) response.body().getData();
                            ArrayList<Post_Detail_Item_Data> dataArrayList = new ArrayList<>();

                            for (FavoriteListCallBack.Data items :dataArrayList2){
                                dataArrayList.add(items.getPost());
                            }

                            if (dataArrayList.size() == 0) {
                                allItemLoaded = true;
                                //Toast.makeText(getApplicationContext(), "마지막 글입니다", Toast.LENGTH_LONG).show();
                            } else {
                                commonSearchMeListAdapter.addItems(dataArrayList);
                                index = commonSearchMeListAdapter.getItemCount() - 1;
                            }

                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option_item) {
        switch (option_item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(option_item);
        }
    }
}
